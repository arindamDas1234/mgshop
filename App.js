import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer } from '@react-navigation/native';

import { createStackNavigator } from '@react-navigation/stack';

import { createDrawerNavigator } from '@react-navigation/drawer';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Icon from 'react-native-vector-icons/dist/FontAwesome';



const Stack = createStackNavigator();

const Drawer = createDrawerNavigator();

const Tab = createBottomTabNavigator();

//  flash screens import section /

import HomeScreen from './src/screens/Home';

//  import User Profile Screen 

import Profile from './src/screens/Profile';

import SettingScreen from './src/screens/setting';

import NewScreen from './src/screens/newScreen';

import FlashScreen from './src/screens/flashScreen';

import OtpVerification from './src/screens/otpVerification';

import LocationScreen from './src/screens/locationDemo';

import ShopByCategory from './src/screens/shopByCategory';

import ProductsDetails from './src/screens/productDetails';

import Search from './src/screens/searchResult';

import FetcherProducts from './src/screens/featureDetails';

import FetcherProducts2 from './src/screens/featureProduct2';

import Address1 from './src/screens/address1';

import Addaddress from './src/screens/addAddress';

import EditAddress from './src/screens/editAddress';

import CheckOut from './src/screens/checkout';

import ThankYouPage from './src/screens/ThankYou';

import OrderScreen from './src/screens/order';

import Invoice from './src/screens/invoice';

import EditOrder from './src/screens/editOrder';



import CartShow from './src/screens/cartShow';
import { Title } from 'react-native-paper';

//  drawer nanvigation function 

// function DrawerRoute(){
// return(
//   <Drawer.Navigator
//     drawerContentOptions={{
//       activeTintColor: '#fff',
//       tintColor: '#fff',
//       itemStyle: { marginVertical: 30, marginBottom: 13, marginTop: 10 },
//       labelStyle: {
//         color: '#fff',
//         justifyContent: 'space-around',
//       },
//     }}
//     statusBarAnimation
//     drawerStyle={{
//       backgroundColor: '#2962ff',
//       width: 250,
//     }}
//   >
// <Drawer.Screen name="Home" component={HomeScreen} />
//   </Drawer.Navigator>
// )
// }

const config = {
  animation: 'spring',
  config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};

function TabRouter() {
  return (
    <Tab.Navigator  tabBarOptions={{
      style: {
        backgroundColor: '#fefdfd'
      },
      activeTintColor:'#3f51b5'
    }} >
      <Tab.Screen name="HomeScreen" component={HomeScreen} options={{
        tabBarIcon: () => <Icon name="home" size={25} color='#3f51b5' />
      }} />
      <Tab.Screen name="Profile" component={Profile} options={{
        tabBarIcon: () => <Icon name="user" size={25} color='#3f51b5' />
      }} />
      <Tab.Screen name="Setting" component={SettingScreen} options={{
        tabBarIcon: () => <Icon name="cog" size={25} color='#3f51b5' />
      }} />
      {/* <Tab.Screen name="Cart" component={CartShow} options={{
        tabBarIcon: () => <Icon name="shopping-cart" size={25} color='#3f51b5' />,
        Title:'title'
      }}
        /> */}
      <Tab.Screen name="Order" component={OrderScreen} options={{
        tabBarIcon: () => <Icon name="truck" size={25} color='#3f51b5' />
      }} />
    </Tab.Navigator>
  )
}

function SplashScrren() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="FlashSreen" component={FlashScreen} options={{
        headerShown:false
      }} />
    </Stack.Navigator>
  )
}

function StackRouter() {

  return (
    <Drawer.Navigator
      overlayColor="transparent"
      drawerType='back'
      drawerContentOptions={{
        
        activeTintColor: '#fff',
        tintColor: '#fff',
        itemStyle: { marginVertical: 30, marginBottom: 13 },
        labelStyle: {
          color: '#fdfefe',
          justifyContent: 'space-around',
        },
      
      }}
      statusBarAnimation
      drawerStyle={{
        backgroundColor: '#6573c3',
      }}>
      <Drawer.Screen
        name="Home"
        component={TabRouter}
        options={{
          drawerIcon: () => <Icon name="home" color="#fefdfd" size={20} />,
        }}
      />
      <Drawer.Screen
        name="Cart"
        component={CartShow}
        options={{
          drawerIcon: () => (
            <Icon name="shopping-cart" color="#fefdfd" size={20} />
          ),
        }}
      />
      <Drawer.Screen
        name="addressShow"
        component={Address1}
        options={{
          drawerIcon: () => (
            <Icon name="address-book-o" color="#fefdfd" size={20} />
          ),
        }}
      />
    </Drawer.Navigator>
  );
}

const App = () => {
  return (
    
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Flash" component={SplashScrren} options={{
          headerShown: false
        }} />
        <Stack.Screen
          name="OtpVerify"
          component={OtpVerification}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="LocationSelect"
          component={LocationScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="HomeScreen"
          component={StackRouter}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen name="NewScreen" component={NewScreen} />
        <Stack.Screen
          name="shopByCategory"
          component={ShopByCategory}
          options={{
            // headerShown: true,
            headerTitle: 'Shop By Category',
            headerTitleStyle: {
              marginHorizontal: 35,
            },
          }}
        />
        <Stack.Screen
          name="ProductsDetails"
          component={ProductsDetails}
          options={{
            headerShown: false,
            headerTitle: 'Products',
            headerTitleStyle: {
              marginHorizontal: 70,
            },
          }}
        />
        <Stack.Screen
          name="Search"
          component={Search}
          options={{
            transitionSpec: {
              open: config,
              close: config,
            },
            headerShown: true,

            headerTitle: 'Search Result',
            headerTitleStyle: {
              marginHorizontal: 50,
            },
          }}
        />
        <Stack.Screen
          name="FeatcherProducts"
          component={FetcherProducts}
          options={{
            headerShown: false,
            headerTitle: 'Products',
            headerTitleStyle: {
              marginHorizontal: 70,
            },
          }}
        />
        <Stack.Screen
          name="FetcherProducts2"
          component={FetcherProducts2}
          options={{
            headerShown: false,
            headerTitle: 'Products',
            headerTitleStyle: {
              marginHorizontal: 70,
            },
          }}
        />
        <Stack.Screen name="Addaddress" component={Addaddress} options={{
          headerShown: false
        }}
        />

        <Stack.Screen name="EditAddress" component={EditAddress} options={{
          headerShown: false
        }}
        />

        <Stack.Screen name="Checkout" component={CheckOut} options={{
          headerShown: false
        }}
        />
        <Stack.Screen name="ThanyouPage" component={ThankYouPage} options={{
          headerShown: false
        }} />

        <Stack.Screen name="Invoice" component={Invoice} options={{
          headerShown:false
        }} />

        <Stack.Screen name="EditOrder" component={EditOrder} options={{
          headerShown:false
        }}
          />
      </Stack.Navigator>
     
    </NavigationContainer>
  );
};


export default App;