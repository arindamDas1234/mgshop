import React, { Component } from 'react';

import { StyleSheet, View, TouchableOpacity, Image, StatusBar,Text } from 'react-native';

export default class ThankyouPage extends Component{
	render() {
		return (
			<View style={{ flex: 1 }}>
				<View
					style={styles.container}>

					<View style={styles.headerview}>

					</View>

					<View style={{ justifyContent: 'center', alignItems: 'center', padding: 33 }}>
						<Text style={{ fontSize: 26, fontFamily: 'lucida grande', color: '#000000', fontWeight: '500', marginTop: 14, textAlign: 'center' }}>
							Success!
      </Text>
						<Image style={{ width: 111, height: 111, marginTop: 15 }}
							source={require('../image/right.png')}
						/>
						<Text style={{ fontSize: 16, fontFamily: 'lucida grande', color: '#000000', fontWeight: '500', marginTop: 14 }}>
							Thank you for shopping
      </Text>
						<Text style={{ color: '#444444', fontFamily: 'lucida grande', marginTop: 5, textAlign: 'center' }}>
							We've recieved your list. We will{"\n"}procure your items fresh in the{"\n"}morning and deliver at your{"\n"}doorstep.
      </Text>
						<TouchableOpacity style={styles.buttoncontainer}
							onPress={ ()=> this.props.navigation.navigate("HomeScreen") }>
							<Text style={styles.buttonText}>Back to Shop</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.buttoncontainer2}
							onPress={ ()=> alert("success") }>
							<Text style={{ textAlign: 'center', fontFamily: 'lucida grande', color: '#000000', fontWeight: '900' }}>See your orders</Text>
						</TouchableOpacity>
					</View>
				</View>

			</View>
		)
	}
}


const styles = StyleSheet.create({

	container: {
		flex: 1,
		backgroundColor: '#ffffff',
	},
	logo1: {
		width: 40,
		height: 40,
		margin: 10
	},
	headercontainer: {
		height: 83,
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: '#4267B2'
	},
	drawertextstyle: {
		margin: 16,
		textAlign: 'left',
		fontWeight: '300'
	},
	contentContainer: {
		paddingVertical: 20
	},
	welcome2: {
		fontSize: 16,
		textAlign: 'center',
		fontWeight: '100',
		color: '#ffffff',

	},
	logo5: {
		marginLeft: 21,
		width: 20,
		alignContent: 'center',
		alignItems: 'center',
		height: 15
	},
	headerview: {
		flexDirection: 'row',
		height: 45,
		backgroundColor: '#ffffff',
		alignItems: 'center'
	},
	buttoncontainer: {
		borderWidth: 1,
		borderRadius: 3,
		borderColor: '#4267B2',
		borderBottomWidth: 0,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 5 },
		shadowOpacity: 0.1,
		shadowRadius: 2,
		elevation: 3,
		backgroundColor: '#4267B2',
		paddingVertical: 15,
		width: '100%',
		marginTop: 24
	},
	buttoncontainer2: {
		borderWidth: 1,
		borderRadius: 3,
		borderColor: '#4267B2',
		borderBottomWidth: 0,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 5 },
		shadowOpacity: 0.1,
		shadowRadius: 2,
		elevation: 3,
		backgroundColor: '#ffffff',
		paddingVertical: 15,
		width: '100%',
		marginTop: 24
	},
	buttonText: {
		textAlign: 'center',
		color: '#FFFFFF',
		fontFamily: 'lucida grande',
		fontWeight: '900'
	},
});