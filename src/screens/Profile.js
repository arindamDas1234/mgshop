import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';


export default class ProfileScreen extends Component{
	render(){
		return(
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
				<TouchableOpacity onPress={()=> this.props.navigation.replace("NewScreen")} >
					<Text style={{ textAlign: 'center' }} >This is Profile Section</Text>
				</TouchableOpacity>
       
			</View>
		)
	}
}