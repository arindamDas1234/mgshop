import React, { Component } from 'react';

import { StyleSheet, View, TouchableOpacity, Text, StatusBar, FlatList, AsyncStorage } from 'react-native';

import { Header, Divider  } from 'react-native-elements';
import { IconButton } from 'react-native-paper';


import NetInfo from '@react-native-community/netinfo';

export default class EditComponent extends Component{

	constructor(props) {
		super(props);

		this.state = {
			cuponCheck: false,
			cupon: this.props.route.params.cupon_value,
			order_id:this.props.route.params.order_id_value,
			user_id: "",
			order_data: {},
			order_data_ites:[]
		}
	}

	componentDidMount() {
		this._unsubscribe = this.props.navigation.addListener('focus', () => {
			this.getOrderData();
		} )
	}

	getOrderData = async () => {
		await AsyncStorage.getItem('user_id').then(result => {
			if (result) {
				this.getOrders();
				this.setState({
					user_id:result
				})
				if ( this.state.cupon == "" ) {
					this.setState({
						cuponCheck: false
					});
				} else {
					this.setState({
						cuponCheck:true
					})
				}
			} else {
				console.log(error);
			}
		})
	}

	getOrders = () => {
		NetInfo.fetch().then(status => {
			if (status.isConnected) {
				fetch('https://sites.mobotics.in/my-shop/api/orders/order_items', {
					method: 'POST',
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/x-www-form-urlencoded',
					},
					body: "order_id=" + this.state.order_id + "&user_id=" + this.state.user_id
				}).then(response => response.json())
					.then(result => {
						console.log(result.data);
						if (result == "") {
							alert("NO Data");
						} else {
							// this.setState({
							// 	order_data:result
							// })
					}
				})
			} else {
				alert("No Internet Connection")
			}
		} )
	}

	render() {
		return (
			<View >
				<StatusBar
					barStyle="light-content"
					hidden={false}
					translucent={true}
					backgroundColor="#3f51b5"
				/>
				<Header
					leftComponent={
						<IconButton
							icon="arrow-left"
							size={20}
							color='#3f51b5'

							onPress={()=> this.props.navigation.goBack(null) }
						/>
					}
					centerComponent={{ text: 'Edit Orders', style: { color: 'black', fontSize:20, fontWeight:'bold' } }}
					rightComponent={{ icon: 'home', color: '#fff' }}

					containerStyle={{
						backgroundColor:'#fff'
					}}
				/>
				<Text style={{ fontSize: 25, fontWeight: 'bold', marginLeft: 40, marginTop: 30 }} >Edit Orders</Text>
				<Text style={{ fontSize: 18, fontWeight: 'bold', marginTop: 14, marginLeft: 40 }} >id</Text>
				<Divider style={{ backgroundColor: 'black', marginTop: 18 }} />

				<FlatList  />
			</View>
		)
	}
}