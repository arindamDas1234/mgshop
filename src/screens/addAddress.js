import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  Dimensions, KeyboardAvoidingView,
  AsyncStorage
} from 'react-native';

import {
  Header, Divider, Card, CheckBox, Button, Input
} from 'react-native-elements';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

import { IconButton, Colors, ActivityIndicator } from 'react-native-paper';

import NetInfo from '@react-native-community/netinfo';

import RadioGroup from 'react-native-custom-radio-group';
import { Value } from 'react-native-reanimated';

import AwesomeAlert from 'react-native-awesome-alerts';




export default class Addaddress extends Component {
  constructor(props){
    super(props)

    this.state = {
      radioGroupList: [
        {
          "label": "Home",
          "value":"Home"
        },
        {
          "label": "Office",
          "value":"Office"
        },
        {
          "label": "Work",
          "value":"work"
        },

      ],
      status: false,
      nick_name_value: "",
      username_validation: true,
      add_validation: true,
      pincode_validation: true,
      landmark_validation: true,
      nick_name_validation: true,
      username: "",
      add: "",
      pincode: "",
      landMark: "",
      Ins: "",
      show_alert: false,
      alert_message:""
    }
  }

  checkValue = (value) => {
    if (value == "work") {
        this.setState({
          status:true
        })
    } else {
      this.setState({
        nick_name_value: value,
        status:false
      })
     }
  } 

  addAddress = () => {
    if (this.state.username == "") {
      this.setState({
        username_validation:false
      })
    } else if (this.state.add == "") {
      this.setState({
        add_validation:false
      })
    } else if (this.state.landMark == "") {
      this.setState({
        landmark_validation:false
      })
    } else if (this.state.pincode == "") {
      this.setState({
        pincode_validation:false
      })
    } else if (this.state.nick_name_value == "") {
      alert("Please Select Your Deliver Address")
    } else {
      NetInfo.fetch().then(status => {
        if (status.isConnected) {
          AsyncStorage.getItem('user_id').then(result => {
            if (result) {
              fetch('http://sites.mobotics.in/my-shop/api/address', {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/x-www-form-urlencoded',
                },
                body:
                  'user_id=' +
                  result +
                  '&name=' +
                  this.state.username +
                  '&address=' +
                  this.state.add +
                  '&pin=' +
                  this.state.pincode +
                  '&land_mark=' +
                  this.state.landMark +
                  '&delivery_instructions=' +
                  this.state.Ins +
                  '&nick_name_for_address=' +
                  this.state.nick_name_value,
              }).then(response => response.json())
                .then(result => {
                  console.log(result)
                  if (result.success) {
                    this.props.navigation.navigate(
                      'addressShow',
                    );
                  } else {
                    this.setState({
                      show_alert: true,
                      alert_message: result.errors
                    })
                  }
                }).catch(error => {
                  console.log(error);
                });
          }
        })
      }
    })
    }
  }
	render() {
		return (
      <View style={{backgroundColor: '#fdfefe', height:1000}}>
        <View style={{backgroundColor: '#fdfefe'}}>
          <StatusBar
            barStyle="dark-content"
            hidden={false}
            translucent={true}
          />
          <Header
            placement="left"
            leftComponent={
              <IconButton
                icon="arrow-left"
                onPress={this.handleDrawer}
                color={Colors.blue800}
                size={20}
                onPress={() => this.props.navigation.goBack(null)}
              />
            }
            centerComponent={
              <Text
                style={{
                  textAlign: 'center',
                  marginHorizontal: 60,
                  fontSize: 20,
                  fontWeight: 'bold',
                }}>
                Add Address
              </Text>
            }
            containerStyle={{
              backgroundColor: '#fff',
            }}
          />
          <KeyboardAvoidingView>
            {this.state.username_validation ? (
              <Input
                placeholder="Enter username"
                onChangeText={value => this.setState({username: value})}
              />
            ) : (
              <Input
                placeholder="Enter username"
                onChangeText={value => this.setState({username: value})}
                errorMessage="Please Enter Your valid User name"
                errorStyle="red"
              />
            )}
            {this.state.add_validation ? (
              <Input
                placeholder="Enter Complete Address"
                onChangeText={value => this.setState({add: value})}
              />
            ) : (
              <Input
                placeholder="Enter Complete Address"
                onChangeText={value => this.setState({add: value})}
                errorMessage="Please Enter Your Valid Address"
                errorStyle="red"
              />
            )}

            {this.state.landmark_validation ? (
              <Input
                placeholder="Pine code"
                onChangeText={value => this.setState({pincode: value})}
              />
            ) : (
              <Input
                placeholder="Pine code"
                onChangeText={value => this.setState({pincode: value})}
                errorMessage="Please Enter Your Valid Pincode"
              />
            )}

            {this.state.landmark_validation ? (
              <Input
                placeholder="Enter LandMark"
                onChangeText={value => this.setState({landMark: value})}
              />
            ) : (
              <Input
                placeholder="Enter LandMark"
                onChangeText={value => this.setState({landMark: value})}
                errorMessage="Please Enter A valid land Mark"
                errorStyle="red"
              />
            )}

            <Input
              placeholder="Delivery Instructions"
              onChangeText={value => this.setState({Ins: value})}
            />
            <Text style={{marginLeft: 10}}>Nick name for Address</Text>

            <View style={{marginTop: 20}}>
              <RadioGroup
                radioGroupList={this.state.radioGroupList}
                initialValue="Home"
                onChange={value => {
                  this.checkValue(value);
                }}
                buttonContainerActiveStyle={{backgroundColor: '#4267B2'}}
                buttonTextActiveStyle={{
                  color: '#ffffff',
                  fontFamily: 'lucida grande',
                }}
                buttonContainerInactiveStyle={{backgroundColor: '#dddddd'}}
                buttonTextInactiveStyle={{
                  color: '#888888',
                  fontFamily: 'lucida grande',
                }}
                buttonContainerStyle={{
                  color: '#dddddd',
                  backgroundColor: '#dddddd',
                  borderColor: '#ffffff',
                  margin: 5,
                  width: 111,
                }}
              />

              {this.state.status ? (
                <Input placeholder="Enter Your Address Details" onChangeText={(value) => this.setState({
                  nick_name_value:value
                })} />
              ) : (
                <View style={{flex: 1, marginTop: 10}}>
                  <ActivityIndicator color="#fff" />
                </View>
              )}
            </View>
            <Button
              title=" Save Address "
              onPress={this.addAddress}
              type="outline"
              containerStyle={{
                width: 300,
                marginLeft: 28,
                marginTop: 30,
              }}
            />
          </KeyboardAvoidingView>
        </View>
        <AwesomeAlert
          show={this.state.show_alert}
          showProgress={ this.state.show_alert }
          title="AwesomeAlert"
          message={this.state.alert_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="No, cancel"
          confirmText="Yes, delete it"
          confirmButtonColor="#DD6B55"
          onCancelPressed={() => {
            this.setState({
              show_alert: false,
            });
          }}
          onConfirmPressed={() => {
            this.setState({
              show_alert:false
            })
          }}
        />
      </View>
    );
	}
}