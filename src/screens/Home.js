import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity  } from 'react-native';

import AnimatedSplash from "react-native-animated-splash-screen";

// import HomeChild Screens /

import HomeChildScreens from './HomeChildScreens';

//  import Login Screen 

import LoginScreen from './Login';

export default class FlashMessages extends Component{
	constructor(props) {
		super(props)

		this.state = {
			isLoading:false
		}
	}

	componentDidMount() {
		setTimeout(() => {
			this.setState({
				isLoading: true
			})
		}, 7500);
	}


	openDrawerFunction = () => {
		this.props.navigation.openDrawer();
	}

	render() {
		return (
			<HomeChildScreens onDrawer={this.openDrawerFunction}
				navigation={this.props.navigation}
			/>

		)
	}
}