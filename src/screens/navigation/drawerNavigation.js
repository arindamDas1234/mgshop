import React from "react";

import { createDrawerNavigator } from "@react-navigation/drawer";



const Drawer = createDrawerNavigator();



import Address1 from './src/screens/address1';

import Addaddress from './src/screens/addAddress';

import EditAddress from './src/screens/editAddress';

import CheckOut from './src/screens/checkout';

import ThankYouPage from './src/screens/ThankYou';

import FlashScreen2 from './src/screens/falshScreen2';



import CartShow from './src/screens/cartShow';

const DrawerNavigator = () => {
	return (
		
			<Drawer.Navigator
				drawerContentOptions={{
					activeTintColor: '#fff',
					tintColor: '#fff',
					itemStyle: { marginVertical: 30, marginBottom: 13 },
					labelStyle: {
						color: '#fff',
						justifyContent: 'space-around',
					},
				}}
				statusBarAnimation
				drawerStyle={{
					backgroundColor: '#2962ff',
				}}>
				<Drawer.Screen
					name="Home"
					component={StackRouter}
					options={{
						drawerIcon: () => <Icon name="home" color="#fff" size={20} />,
					}}
				/>
				<Drawer.Screen
					name="Cart"
					component={CartShow}
					options={{
						drawerIcon: () => (
							<Icon name="shopping-cart" color="#fff" size={20} />
						),
					}}
				/>
				<Drawer.Screen
					name="addressShow"
					component={Address1}
					options={{
						drawerIcon: () => (
							<Icon name="address-book-o" color="#fff" size={20} />
						),
					}}
				/>
			</Drawer.Navigator>
		
	);
}

export default DrawerNavigator;