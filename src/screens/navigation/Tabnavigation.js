// ./navigation/TabNavigator.js

import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { MainStackNavigator, ContactStackNavigator } from "./StackNavigator";

const Tab = createBottomTabNavigator();

const Tab = createBottomTabNavigator();


import Profile from './src/screens/Profile';

import SettingScreen from './src/screens/setting';

import NewScreen from './src/screens/newScreen';

const BottomTabNavigator = () => {
	return (
		<Tab.Navigator tabBarOptions={{
			style: {
				backgroundColor: '#fff'
			}
		}} >
			<Tab.Screen name="HomeScreen" component={HomeScreen} options={{
				tabBarIcon: () => <Icon name="home" size={25} color='#3f51b5' />
			}} />
			<Tab.Screen name="Profile" component={Profile} options={{
				tabBarIcon: () => <Icon name="user" size={25} color='#3f51b5' />
			}} />
			<Tab.Screen name="Setting" component={SettingScreen} options={{
				tabBarIcon: () => <Icon name="cog" size={25} color='#3f51b5' />
			}} />
			{/* <Tab.Screen name="Cart" component={CartShow} options={{
        tabBarIcon: () => <Icon name="shopping-cart" size={25} color='#3f51b5' />,
        Title:'title'
      }}
        /> */}
		</Tab.Navigator>
	);
};

export default BottomTabNavigator;