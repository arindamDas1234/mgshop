import React, { Component } from 'react';

import { StyleSheet, View, Text, Touchableopacity } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

import PhoneVerufy from './src/screens/phoneVerify';

import OtpVerification from './src/screens/otpVerification';

import LocationScreen from './src/screens/locationDemo';

import ShopByCategory from './src/screens/shopByCategory';

import ProductsDetails from './src/screens/productDetails';

import Search from './src/screens/searchResult';

import FetcherProducts from './src/screens/featureDetails';

import FetcherProducts2 from './src/screens/featureProduct2';

const MainStackNavigator = () => {
	return (
		<Stack.Navigator
			// screenOptions={{
			// 	headerStyle: {
			// 		backgroundColor: "#9AC4F8",
			// 	},
			// 	headerTintColor: "white",
			// 	headerBackTitle: "Back",
			// }}
		>
			<Stack.Navigator>
				<Stack.Screen
					name="FlashScreen2"
					component={FlashScreen2}
					options={{
						headerShown: false,
					}}
				/>
				<Stack.Screen
					name="Phoneverify"
					component={PhoneVerufy}
					options={{
						headerShown: false,
					}}
				/>
				<Stack.Screen
					name="OtpVerify"
					component={OtpVerification}
					options={{
						headerShown: false,
					}}
				/>
				<Stack.Screen
					name="LocationSelect"
					component={LocationScreen}
					options={{
						headerShown: false,
					}}
				/>
				<Stack.Screen
					name="HomeScreen"
					component={TabRouter}
					options={{
						headerShown: false,
					}}
				/>
				<Stack.Screen name="NewScreen" component={NewScreen} />
				<Stack.Screen
					name="shopByCategory"
					component={ShopByCategory}
					options={{
						// headerShown: true,
						headerTitle: 'Shop By Category',
						headerTitleStyle: {
							marginHorizontal: 35,
						},
					}}
				/>
				<Stack.Screen
					name="ProductsDetails"
					component={ProductsDetails}
					options={{
						headerShown: true,
						headerTitle: 'Products',
						headerTitleStyle: {
							marginHorizontal: 70,
						},
					}}
				/>
				<Stack.Screen
					name="Search"
					component={Search}
					options={{
						transitionSpec: {
							open: config,
							close: config,
						},
						headerShown: true,

						headerTitle: 'Search Result',
						headerTitleStyle: {
							marginHorizontal: 50,
						},
					}}
				/>
				<Stack.Screen
					name="FeatcherProducts"
					component={FetcherProducts}
					options={{
						headerShown: true,
						headerTitle: 'Products',
						headerTitleStyle: {
							marginHorizontal: 70,
						},
					}}
				/>
				<Stack.Screen
					name="FetcherProducts2"
					component={FetcherProducts2}
					options={{
						headerShown: true,
						headerTitle: 'Products',
						headerTitleStyle: {
							marginHorizontal: 70,
						},
					}}
				/>
				<Stack.Screen name="Addaddress" component={Addaddress} options={{
					headerShown: false
				}}
				/>

				<Stack.Screen name="EditAddress" component={EditAddress} options={{
					headerShown: false
				}}
				/>

				<Stack.Screen name="Checkout" component={CheckOut} options={{
					headerShown: false
				}}
				/>
				<Stack.Screen name="ThanyouPage" component={ThankYouPage} options={{
					headerShown: false
				}} />
			</Stack.Navigator>
		</Stack.Navigator>
	);
}

export default MainStackNavigator