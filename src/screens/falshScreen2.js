import React, { Component } from 'react';

import { StyleSheet, View, Text, Touchableopacity, Image, Animated, Easing } from 'react-native';

export default class FlashMessage  extends Component {

	constructor() {
		super()
		this.animatedValue1 = new Animated.Value(0)
		this.animatedValue2 = new Animated.Value(0)
		this.animatedValue3 = new Animated.Value(0)
	}

	componentDidMount () {
		this.animate()
		setTimeout(() => {
			this.props.navigation.navigate("Phoneverify");
		}, 3000);
}animate () {
  this.animatedValue1.setValue(0)
  this.animatedValue2.setValue(0)
  this.animatedValue3.setValue(0)
  const createAnimation = function (value, duration, easing, delay = 0) {
    return Animated.timing(
      value,
      {
        toValue: 1,
        duration,
        easing,
        delay
      }
    )
  }
  Animated.parallel([
    createAnimation(this.animatedValue1, 2000, Easing.ease),
    createAnimation(this.animatedValue2, 1000, Easing.ease, 1000),
    createAnimation(this.animatedValue3, 1000, Easing.ease, 2000)        
  ]).start()
	}
	
	render() {
		const scaleText = this.animatedValue1.interpolate({
			inputRange: [0, 1],
			outputRange: [0.5, 1]
		})
		const spinText = this.animatedValue2.interpolate({
			inputRange: [0, 0.5],
			outputRange: ['0deg', '720deg']
		})
		const introButton = this.animatedValue3.interpolate({
			inputRange: [0, 1],
			outputRange: [-200, 400]
		})

		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fdfefe' }} >
				<Animated.View
					style={{ transform: [{ scale: scaleText }] }}>
					<Image source={require('../image/logo.jpg')} style={{ width: 200, height: 200 }} />
				</Animated.View>
				
			</View>
		)
	}
}