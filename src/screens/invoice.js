import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, StatusBar, Image, AsyncStorage, FlatList, ListView } from 'react-native';

import { Header, Divider, Button } from 'react-native-elements';

import { IconButton, Colors, ActivityIndicator } from 'react-native-paper';


import NetInfo from '@react-native-community/netinfo';
import { Value } from 'react-native-reanimated';
import { Row } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';

import DialogBox from 'react-native-dialogbox';

export default class Invoice extends Component {

	constructor(props) {
		super(props)

		this.state = {
			cart_items: [],
			totalPrice: "",
			delivery_time: "",
			delivery_date: "",
			delivery_Address: ""
		}
	}

	renderItems = () => {
		this.state.cart_items.forEach(value => {
			if (value) {
				alert(value)
				return (
					<View>
						<Text>Hello</Text>
					</View>
				)
			}
		})
	}

	confirmOrder = () => {

		NetInfo.fetch().then(status => {
			if (status.isConnected) {
				AsyncStorage.getItem('user_id').then(result => {
					if (result) {
						fetch('https://sites.mobotics.in/my-shop/api/checkout/cash_on_delivery', {
							method: "POST",
							headers: {
								Accept: 'application/json',
								'Content-Type': 'application/x-www-form-urlencoded',
							},
							body: "payment_gateway=cashonndelivery" + "&ordered_from=android" + "&shipping_address=" + this.props.route.params.value
								+ "&billing_address=" + this.props.route.params.value + "&user_id=" + result
						}).then(response => response.json())
							.then(result => {
								if (result.status) {
									this.props.navigation.navigate("ThanyouPage")
								} else {
									this.dialogbox.confirm({
										title: "Alert",
										content: result.text,
										ok: {
											text: "Yes",
											style: {
												color: 'red'
											},
											callback: () => {
												this.props.navigation.navigate("HomeScreen");
											}
										}
									})
								}
							}).catch(error => {
								console.log(error);
							})
					}
				})
			} else {
				alert("No Internet connection")
			}
		})
	}

	componentDidMount() {
		const delivery_date = this.props.route.params.delivery_date;
		const delivery_time = this.props.route.params.delivery_time;
		const address = this.props.route.params.delivery_Address;


		NetInfo.fetch().then(status => {
			if (status.isConnected) {
				AsyncStorage.getItem('user_id').then(result => {
					if (result) {
						fetch('https://sites.mobotics.in/my-shop/api/cart/items?user_id=' + result, {
							method: 'GET',
							headers: {
								'Content-Type': 'application/json'
							},
						}).then(response => response.json())
							.then(result => {
								;
								this.setState({
									cart_items: result.data.items,
									totalPrice: result.data.total,
									delivery_date: delivery_date,
									delivery_time: delivery_time,
									delivery_Address: address
								})
							}).catch(error => {
								console.log(error);
							});
					}
				})
			} else {
				alert("No internet Connection")
			}
		})
	}

	render() {

		return (
			<View
				style={{
					justifyContent: 'center',
					alignItems: 'center',
					flex: 1,
					backgroundColor: '#fdfefe',
				}}>
				<StatusBar
					barStyle="light-content"
					hidden={false}
					translucent={true}
				/>

				<Header
					placement="left"
					leftComponent={
						<IconButton
							icon="arrow-left"
							onPress={this.handleDrawer}
							color={Colors.blue800}
							size={20}
							onPress={() =>
								this.props.navigation.goBack({ Home: 'Addaddress' })
							}
						/>
					}
					centerComponent={
						<Text
							style={{
								textAlign: 'center',
								marginHorizontal: 70,
								fontSize: 20,
								fontWeight: 'bold',
							}}>
							{' '}
             Invoice
						</Text>
					}
					containerStyle={{
						backgroundColor: '#fff',
					}}
				/>

				<ScrollView showsVerticalScrollIndicator={false} >

					<View>
						<Text style={{ marginRight: 220, marginTop: 20, color: 'grey' }}>
							Delivery Date
          </Text>
						<Text style={{ fontWeight: 'bold', marginTop: 10 }}>
							{this.state.delivery_date}
						</Text>
						<Text style={{ marginRight: 220, marginTop: 5, color: 'grey' }}>
							{this.state.delivery_time}
						</Text>
						<Divider
							style={{ backgroundColor: 'grey' }}
							style={{ marginBottom: 15, marginTop: 20, elevation: 5 }}
						/>
						<Text style={{ marginRight: 220, marginTop: 20, color: 'grey' }}>
							Address
          </Text>
						<Text style={{ fontWeight: 'bold', marginTop: 5 }}>
							{this.state.delivery_Address}
						</Text>
						<Text style={{ marginRight: 220, marginTop: 5, color: 'grey' }}>
							{this.props.route.params.name}
						</Text>
						<Divider
							style={{ backgroundColor: 'grey' }}
							style={{ marginBottom: 15, marginTop: 20, elevation: 5 }}
						/>
						<Text style={{ fontWeight: 'bold', marginBottom: 10 }}>
							Payment Details
          </Text>
						<View style={{ flexDirection: 'row' }} >
							<Image
								source={require('../image/cod.png')}
								style={{ height: 23, width: 40, marginRight: 10 }}
							/>
							<View>
								<Text style={{ color: 'grey' }} > Cash On Delivery </Text>
							</View>
						</View>
						<Divider
							style={{ backgroundColor: 'grey' }}
							style={{ marginBottom: 15, marginTop: 20, elevation: 5 }}
						/>


						<Text style={{ fontSize: 18, fontWeight: 'bold' }}>Items</Text>

						<FlatList data={this.state.cart_items}
							renderItem={(items) => {
								return (
									<View>
										<Text>{items.item.product_name}</Text>
									</View>
								)
							}}
						/>


						<View style={{ flexDirection: 'row' }} >
							<Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: 20, }} >Total Price:</Text>
							<View>
								<Text style={{ marginLeft: 10, marginTop: 23 }}  > {this.state.totalPrice} </Text>
							</View>

						</View>

					</View>

				</ScrollView>
			
				<DialogBox
					ref={dialogbox => {
						this.dialogbox = dialogbox;
					}}
				/>
			</View>
		);
	}
}


const styles = StyleSheet.create({
	title: {
		fontSize: 25,
		color: '#fff'
	},
	listItem: {
		height: 120,
		margin: 10,
		flexDirection: 'row',
	},
	image: {
		height: 80,
		width: 100,
		flex: 1,
		margin: 10
	},
	itemContainer: {
		height: 130,
		borderBottomWidth: 1,
		borderBottomColor: '#c6c6c6'
	},
	content: {
		flex: 2,
		flexDirection: 'row',
		margin: 10
	},
	details: {
		flex: 2,
	},
	closeBtnWrapper: {
		justifyContent: 'center',
		alignItems: 'center',

	},
	closeBtn: {
		height: 50,
		width: 50,
		borderRadius: 90,
		alignItems: 'center',
		flexDirection: 'column',
		backgroundColor: '#F44336',
		justifyContent: 'center',

	},
	qty: {
		fontWeight: '400',
		fontSize: 15,
	},
	price: {
		fontWeight: '400',
		fontSize: 15,
		color: 'red'
	},
	adTitle: {
		fontWeight: '600',
		fontSize: 16,
		margin: 5
	},

	adContent: {

	},
	buttonContainer: {

	},
	container: {
		flex: 1,
		backgroundColor: '#fff',
		flexDirection: 'column',
	},
	buyMeButton: {
		backgroundColor: '#fff',
		position: 'absolute',
		bottom: 0,
		left: 0,
		right: 0,
		textAlign: 'center',
		alignItems: 'center',
		padding: 20

	},
	buttonText: {
		color: '#fff',
		fontSize: 20,
		fontWeight: '500'
	}
})

const mapStateToProps = (state) => {
	return {
		products: state.productReducer
	};
};

