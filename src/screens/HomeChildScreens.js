import React, { Component,BackHandler  } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, StatusBar, FlatList,Image, Dimensions, ScrollView, ActivityIndicator, AsyncStorage } from 'react-native';

import { Header, Divider, Card, CheckBox, Button  } from 'react-native-elements';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

import { IconButton, Colors } from 'react-native-paper';

import NetInfo from '@react-native-community/netinfo';

import { Container, Content, List, ListItem, Thumbnail, Left, Body, Right, } from 'native-base';

import SearchBar from 'react-native-search-bar';

import DropdownAlert from 'react-native-dropdownalert';

const KEYS_TO_FILTERS = ['title', 'slug'];

import SearchInput, {createFilter} from 'react-native-search-filter';

import DropDownPicker from 'react-native-dropdown-picker';

import NumericInput from 'react-native-numeric-input';

import RNPickerSelect from 'react-native-picker-select';

// import AwesomeAlert from 'react-native-awesome-alerts';


export default class HomeChildComponent extends Component {

	constructor(props) {
		super(props);

		this.state = {
  
      categories:[],
      
      bannerImage: [],
      TopDealsProducts: [],
      products: [],
      cat_by_products:[],
      checkConnection: false,
      combinations: [],
      next: "",
      searchQuery: "",
      searchResult: [],
      active_cart_list: true,
      all_cat_product: [],
      image_Active: false,
      new_slug: "all",
      list_status: false,
      isupdateLoading: false,
      product_qty:"1",
      check_cart_status:false
    };

    
  }

  shopByCategory = (result) => {
    this.setState({
      new_slug: result,
      cat_by_products:[]
    });

    if (this.state.new_slug != result) {
      NetInfo.fetch().then(status => {
        if (status.isConnected) {

          fetch('https://sites.mobotics.in/my-shop/api/category/' + result, {
            method: 'GET'
          }).then(response => response.json())
            .then(result => {

              if (result) {
                this.setState({
                  isupdateLoading: true
                });
                this.setState({
                  cat_by_products: result.data,
                  checkNetworkStatus: true
                })
              }
            }).then(error => {
              console.log(error);
            });
        }else{
          BackHandler.exitApp();
        }
      })
    } else {
      NetInfo.fetch().then(status => {
        if (status.isConnected) {
          this.setState({
            isupdateLoading: true
          })
          fetch('https://sites.mobotics.in/my-shop/api/products', {
            method: 'GET',

          }).then(response => response.json())
            .then(result => {
              this.setState({
                cat_by_products: result.data,
                isupdateLoading: true

              })
              if (result.data.length > 0) {
                result.data.forEach(element => {



                });
              }
            }).catch(error => {
              console.log(error);
            });
        }else{
          BackHandler.exitApp();
        }
      })
}
   
  }

  allProducts = () => {
    this.getCatProducts();
  }

  checkCat = (slug) => {
    
    if (slug == this.state.new_slug) {

      return true;
    } else {
      return false
    }
  }

  componentDidMount() {
    this.getHomeDetails();
    this.getAllCategory();
    this.getProducts();
    this.getCatProducts();
    // this.searchProducts();
  }

  getHomeDetails = async () => {
   await AsyncStorage.getItem('user_id').then(result => {
      if (result) {
        NetInfo.fetch().then(status => {
          if (status.isConnected) {
            fetch('https://sites.mobotics.in/my-shop/api/home?user_id/' +result, {
              method: 'GET',

            }).then(response => response.json())
              .then(result => {
                console.log(result.data.contents.widgets.slider.image);
                if (result) {
                  this.setState({
                    bannerImage: result.data.contents.widgets.slider,
                    TopDealsProducts: result.data.contents.widgets.is_featured_product,
                    checkConnection: true
                  })
                }
              })
          }else{
            BackHandler.exitApp();
          }
        })
       }
     })
  }

  

  //  get products 

  getProducts = () => {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch('https://sites.mobotics.in/my-shop/api/products', {
          method: 'GET',
        
        }).then(response => response.json())
          .then(result => {
          
            if (result.data.length > 0) {
              this.setState({
                isupdateLoading:true
              })
              result.data.forEach(element => {
       
           
                this.setState({
                  products: result.data,
                  combinations: element.combinations[0],
                  next:result.links.next
                })
              });
            }
          }).catch(error => {
            console.log(error);
          });
      }else{
        BackHandler.exitApp()
      }
    })
  }

  getCatProducts = () => {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        this.setState({
          isupdateLoading:true
        })
        fetch('https://sites.mobotics.in/my-shop/api/products', {
          method: 'GET',

        }).then(response => response.json())
          .then(result => {
            this.setState({
              cat_by_products: result.data,
              isupdateLoading: true

            })
          }).catch(error => {
            console.log(error);
          });
      }
    })
  }

  search = (value) => {
    if (this.state.searchQuery == "") {
      this.dropDownAlertRef.alertWithType(
        'error',
        'Error',
        ' Ups ! Enter Your Search Product or Category',
      );
    } else {
      this.props.navigation.navigate("Search", {
        searchValue: this.state.searchQuery
      });
    }
    return;
   
this.setState({
  searchQuery:value
}) 
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch('http://sites.mobotics.in/my-shop/api/search?q=' + this.state.searchQuery, {
          method: 'GET'
        }).then(response => response.json())
          .then(result => {
            result.data.map(value => {
              console.log(data)
            })
            this.setState({
              searchResult: result.data
             
            })
          }).catch(error => {
            // console.log(error);
          });
      }
    });
  }

  getAllCategory = () => {
  
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch('https://sites.mobotics.in/my-shop/api/products/filters', {
          method:'GET',
          headers: {
            'Content-Type':'application/json'
          }
        }).then(response => response.json())
          .then(result => {
            if (result.data) {
              this.setState({
                categories: result.data.category,
                checkConnection:true
            })
          }
          }).catch(error => {
            console.log(error);
        })
      }
    })
  }

  showProducts = (result) => {
    if (result == "") {
      alert("error")
    } else {
      this.props.navigation.navigate("ProductsDetails", {
        productData: result
      })
    }
  }

  detailsProducts = (result) => {
    
    this.props.navigation.navigate("ProductsDetails", {
      productData: result.slug
    })
  }

  addToCart = (data) => {
   
    this.setState({
      check_cart_status: data
    });
    AsyncStorage.getItem('user_id').then(result => {
      if (result) {

        NetInfo.fetch().then(status => {
          if (status.isConnected) {
            fetch('https://sites.mobotics.in/my-shop/api/cart/add', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
              },
              body: "product_price_id=" + data + "&quantity=" + this.state.product_qty + "&user_id=" + result
            })
              .then(data => {
                console.log(data);
                
                if (data.status === 200) {
                  this.setState({
                    cart_button_change: true
                  });
                  alert("Product Adds To cart successfully :) ");
                } else {
                  alert("Ups ! Your Product All Ready Exists In cart Bucket :(")
                  
                }
              }).catch(error => {
                console.log(error);
              });
          }
        })
      } else {

      }
    })
  }

  check_cart_status = (result) => {
    if (result === this.state.check_cart_status) {
      return true;
    } else {
      return false;
     }
  }

  handleDrawer = () => {
    this.props.onDrawer();
  }
  render() {
     const filteredEmails = this.state.searchResult.filter(
       createFilter(this.state.searchQuery, KEYS_TO_FILTERS),
     );
		return (
      <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor:'#fdfefe'}}>
        <DropdownAlert ref={ref => (this.dropDownAlertRef = ref)} />
        <StatusBar
          barStyle="light-content"
          hidden={false}
          translucent={true}
          backgroundColor='#3f51b5'
        />

        <Header
          placement="left"
          leftComponent={
            <IconButton
              icon="menu"
              onPress={this.handleDrawer}
              color={Colors.blue800}
              size={20}
            />
          }
          centerComponent={
            <SearchInput
              onChangeText={term => {
                this.setState({
                  searchQuery:term
                })
              }}
              // style={styles.searchInput}
              placeholder=" Search Your Products and Category "
            />
          }
          rightComponent={
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity>
                <Icon name="search" size={20} color="#3f51b5" style={{ marginRight:15 }} onPress={this.search} />
              </TouchableOpacity>
              <View >
                <Icon name="cart-arrow-down" size={20} color="#3f51b5" onPress={()=> this.props.navigation.navigate("Cart") } />
              </View>
              <View style={{ marginLeft: 10 }} >
                <Icon name="bell-o" size={20} color="#3f51b5"  onPress={() => this.props.navigation.navigate("Cart")} />
              </View>
            </View>
          }
          containerStyle={{
            backgroundColor: '#fff',
          }}
        />

        {this.state.checkConnection ? (
          <ScrollView>
            {filteredEmails.map(email => {

              if (email != "" && this.state.searchQuery != "") {
                return (
                  <TouchableOpacity
                    onPress={() => alert(email.title)}
                    key={email.id}
                  >
                    <View>
                      <Text style={{ marginTop: 20, marginLeft: 20, color: 'rgba(0,0,0,0.5)' }} >{email.title}</Text>

                    </View>
                  </TouchableOpacity>
                );
             }
            })}
            <FlatList
              style={{width: Dimensions.get('window').width}}
              horizontal
              pagingEnabled={true}
              showsHorizontalScrollIndicator={false}
              legacyImplementation={false}
              data={this.state.bannerImage}
              renderItem={(items, index) => {
                console.log(items.item.image);
                return (
                  <Image
                    source={{uri: items.item.image}}
                    style={{
                      height: 200,
                      width: Dimensions.get('window').width,
                      elevation: 10,
                      zIndex: -1,
                    }}
                  />
                );
              }}
              keyExtractor={item => item.id}
            />
            <Text
              style={{
                marginTop: 15,
                textAlign: 'center',
                fontSize: 18,
                marginRight: 140,
                elevation:10,
                color: "#000",
               
              }}>
            Shop By Category
            </Text>
            <View style={{ flexDirection: 'row' }} >
              {
                this.checkCat("all")  ? (
                  <TouchableOpacity onPress={() => this.shopByCategory("all")} >
                    <Image source={(require('../image/allcat2.png'))} style={{
                      marginTop: 20, marginLeft: 10, color: 'blue', height: 40,
                      width: 40,
                      borderRadius: 20,
                      justifyContent: 'space-around',
                      marginLeft: 14 }} />
                </TouchableOpacity>
                ) : (
                    <TouchableOpacity onPress={() => this.shopByCategory("all")} >
                      <Image source={(require('../image/allcat1.png'))} style={{
                        marginTop: 20, marginLeft: 10, color: 'blue', height: 40,
                        width: 40,
                        borderRadius: 20,
                        justifyContent: 'space-around',
                        marginLeft: 14
                      }} />
                    </TouchableOpacity>
                )
             }
              <View>
            {/* all category list */}
                <FlatList
                  data={this.state.categories}
                  style={{ width: Dimensions.get('window').width }}
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  renderItem={items => {
                    console.log(items.item.image_active)
                    return (
                      <TouchableOpacity
                        onPress={() => this.shopByCategory(items.item.slug)}>
                        <View style={{ marginTop: 20 }}>
                          {
                            this.checkCat(items.item.slug) ? (
                              <View>
                                
                                <Image
                                  source={{ uri: items.item.image_active }}
                                  style={{
                                    height: 40,
                                    width: 40,
                                    borderRadius: 20,
                                    justifyContent: 'space-around',
                                    marginLeft: 14,
                                  }}
                                />
                              </View>
                            ) : (
                                <Image
                                  source={{ uri: items.item.image_inactive }}
                                  style={{
                                    height: 40,
                                    width: 40,
                                    borderRadius: 20,
                                    justifyContent: 'space-around',
                                    marginLeft: 14,
                                  }}
                                />
                            )
                         }
                          {/* <Text
                      numberOfLines={2}
                      ellipsizeMode="tail"
                      style={{
                        textAlign: 'center',
                        marginLeft: 4,
                        width:50,
                        marginTop: 10,
                      }}>
                      {items.item.name}
                    </Text> */}
                        </View>
                      </TouchableOpacity>
                    );
                  }}
                />
                {/* end of all category list */}
              </View>
            </View>
            
            <Divider
              style={{backgroundColor: 'grey'}}
              style={{marginBottom: 15, marginTop: 20, elevation: 5}}
            />
        
            {/* all category list by proudct */}
            {/* all category list by proudct */}
            {
              this.state.cat_by_products.length != 0 ? (
                <FlatList data={this.state.cat_by_products}
                  style={{ width:'100%' }}
                  renderItem={(items) => {
                    
                    return (
                      <View
                        style={{
                          flex: 1,
                          borderWidth: 0.3,
                          borderColor: 'grey',
                        }}>
                        <TouchableOpacity onPress={() => this.detailsProducts(items.item)} >
                          <View
                            style={{ flexDirection: 'row' }}>
                            <Image
                              source={{
                                uri: items.item.image.small,
                              }}
                              style={{
                                height: 80,
                                width: 80,
                                borderRadius: 10,
                                elevation: 10,
                                marginTop: 10,
                                color: 'blue',
                              }}
                            />

                            <View
                              style={{
                                width: '30%',
                                marginTop: 10,
                              }}>
                              <Text
                                numberOfLines={1}
                                style={{
                                  textAlign: 'center',
                                  marginTop: 10,
                                  marginLeft: 10,
                                  textWeight: 'bold',
                                  color: 'black',
                                  width: 100,

                                  marginTop: 10,
                                }}>
                                {items.item.title}
                              </Text>
                              {/* <DropDownPicker
                              items={items.item.combinations.map(value => ({
                                label: value.size.substring(0, 18),
                                value:value.id
                              }))}
                             
                              dropDownStyle={{
                                backgroundColor: '#fff',
                              }}
                              itemStyle={{
                                justifyContent: 'center',
                                backgroundColor: '#fff',
                              }}
                              labelStyle={{
                                fontSize: 14,
                                textAlign: 'left',
                                color: 'black',
                              }}
                              defaultIndex={0}
                              placeholder="select variation"
                              containerStyle={{
                                height: 50,
                                width: 100,
                                backgroundColor: '#fff',
                                marginLeft: 10,
                                marginTop: 5,

                              }}
                              style={{
                                borderRadius: 55,
                                borderColor: '#3f51b5',
                                backgroundColor: '#fff',
                              }}
                              onChangeItem={item =>
                                console.log(
                                  item.label,
                                  item.value,
                                ) 
                              }
                            /> */}

                              <RNPickerSelect
                                onValueChange={value =>
                                  console.log(value)
                                }
                                items={items.item.combinations.map(
                                  value => ({
                                    label: value.size.substring(
                                      0, 18,
                                    ),
                                    value: value.size,
                                  }),
                                )}
                                onValueChange={value =>
                                  alert(value)
                                }
                                style={{
                                  borderRadius: 55,
                                  borderColor: '#3f51b5',
                                  backgroundColor: '#fff',
                                }}
                              />
                            </View>
                            <View>
                              <Text
                                style={{
                                  textAlign: 'center',
                                  fontWeight: 'bold',
                                  marginTop: 20,
                                }}>
                                ₹{items.item.price}
                              </Text>

                              <Button
                                title="Add cart"
                                type="outline"
                                onPress={() =>
                                  this.addToCart(
                                    items.item
                                      .combinations[0].id,
                                  )
                                }
                                containerStyle={{
                                  width: 120,
                                  borderRadius: 15,
                                  marginTop: 10,
                                  color: '#fff'
                                }}
                                buttonStyle={{
                                  backgroundColor: '#3f51b5',
                                  color: '#fff'
                                }}
                                titleStyle={{
                                  color: '#fff'
                                }}
                              />
                            </View>
                          </View>
                </TouchableOpacity>

                      </View>
                    );
                  }}
                />
              ) : (
                  <ActivityIndicator animating color='#3f51b5' size="small" style={{ marginRight:140 }}  />
              )
        }
            {/* end of all category list by Product */}
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 10,
                  fontSize: 24,
                  fontWeight: 'bold',
                  elevation: 4,
                  marginLeft: 12,
                }}>
                Top Deals
              </Text>
              <TouchableOpacity>
                <View
                  style={{
                    marginTop: 15,
                    marginHorizontal: 150,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      color: '#3f51b5',
                      textAlign: 'center',
                      fontSize: 14,
                    }}>
                    more
                  </Text>
                  <View style={{marginTop: 6, marginHorizontal: 20}}>
                    <Icon name="arrow-right" color="#3f51b5" size={14} />
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <FlatList
              data={this.state.TopDealsProducts}
              style={{width: Dimensions.get('window').width, marginTop: 15}}
              horizontal
              showsHorizontalScrollIndicator={false}
              renderItem={items => {
                return (
                  <TouchableOpacity
                    onPress={() => this.detailsProducts(items.item)}>
                    <View>
                      <Card style={{elevation: 10}}>
                        <View>

                          <Image
                            source={require('../image/favno.png')}
                            style={{
                              fontSize: 16,

                              left: 10,
                              zIndex: 4000,
                              borderRadius:20,
                              width: 30,
                              height: 20,
                              position: 'absolute',
                              marginLeft: 5,
                            }}
                          />
                          <Image
                            source={{
                              uri: items.item.image.large,
                            }}
                            style={{
                              height: 150,
                              width: 200,
                              justifyContent: 'space-around',
                              marginHorizontal: 15,
                              marginBottom: 15,
                            }}
                          />
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: 20,
                              fontWeight: 'bold',
                            }}>
                            {' '}
                            {items.item.title}{' '}
                          </Text>
                          <Text
                            style={{
                              textAlign: 'center',
                              color: 'grey',
                              fontSize: 14,
                            }}>
                            {' '}
                            {items.item.categories.title}{' '}
                          </Text>

                          <View />
                        </View>
                      </Card>
                    </View>
                  </TouchableOpacity>
                );
              }}
            />
            {/* <Divider
              style={{backgroundColor: 'grey'}}
              style={{marginTop: 10, elevation: 5}}
            /> */}
            

            <Divider
              style={{backgroundColor: 'grey'}}
              style={{marginBottom: 15, marginTop: 20, elevation: 5}}
            />

            <Text
              style={{
                textAlign: 'center',
                fontSize: 20,
                fontWeight: 'bold',
                marginRight: 135,
                marginTop: 10,
              }}>
              You May Also Like
            </Text>

            {
              this.state.products.length == 0 ? (
                <ActivityIndicator color='#3f51b5' size="small" style={{ marginRight: 140, marginTop:30 }} />
              ): (
                <FlatList data = {this.state.products}
              renderItem={(items) => {
              return (
                <View style={{flexDirection: 'row'}}>
                  <Image
                    source={{uri: items.item.image.small}}
                    style={{height: 80, width: 80}}
                  />

                  <View style={{width: '30%'}}>
                    <Text style={{marginTop: 20}}>
                      {' '}
                      {items.item.title}{' '}
                    </Text>

                    <RNPickerSelect
                      onValueChange={value => console.log(value)}
                      items={items.item.combinations.map(
                        value => ({
                          label: value.size.substring(0, 18),
                          value: value.size.substring(0, 18),
                        }),
                      )}
                     
                    />
                  </View>
                  <View>
                    <Text
                      style={{
                        marginTop: 20,
                        textAlign: 'center',
                      }}>
                      {' '}
                      ₹{items.item.price}{' '}
                    </Text>
                    <Button
                      title="Add cart"
                      type="outline"
                      onPress={() =>
                        this.addToCart(
                          items.item.combinations[0].id,
                        )
                      }
                      containerStyle={{
                        width: 120,
                        borderRadius: 15,
                        marginTop: 10,
                        color: '#fff',
                      }}
                      buttonStyle={{
                        backgroundColor: '#3f51b5',
                        color: '#fff',
                      }}
                      titleStyle={{
                        color: '#fff',
                      }}
                    />
                  </View>
                </View>
              );
            }}
            />
              )
           }

            <Button title="Load More"
              containerStyle={{
                marginTop: 20,
                width: 200,
                marginHorizontal: 80,
                marginBottom: 60,
              }}

              type="outline"
            />

          </ScrollView>
        ) : (
          <ActivityIndicator
            animating={true}
            style={{
              marginTop: 250,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            size="large"
            color="#3f51b5"
          />
        )}
      </View>
    );
	}
}