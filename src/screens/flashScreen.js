import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  Animated,
  TouchableOpacity,
  ToastAndroid,
  Alert, StatusBar
} from 'react-native';

import AnimatedSplash from "react-native-animated-splash-screen";

// import HomeChild Screens /

import HomeChildScreens from './HomeChildScreens';

//  import Login Screen 

import LoginScreen from './Login';

import {SharedElement} from 'react-native-motion';

import Icon from 'react-native-vector-icons/FontAwesome';
import { Input, Button } from 'react-native-elements';

import NetInfo from '@react-native-community/netinfo';

import Toast from 'react-native-toast-message';
import _interopRequireDefault from '@babel/runtime/helpers/interopRequireDefault';

export default class FlashMessages extends Component {
                 constructor(props) {
                   super(props);

                   this.state = {
                     button_loading: false,
                     checkConnection: false,
                     mobile_number:'',
                     validation: false
					 };
					 
					  this.animatedValue1 = new Animated.Value(0);
            this.animatedValue2 = new Animated.Value(0);
            this.animatedValue3 = new Animated.Value(0);
  }
  
  // componentWillMount() {
    
  // }

                 componentDidMount() {
                   setTimeout(() => {
                     this.setState({
                       isLoading: true,
                     });
                     this.animate();
				   }, 7500);

                 }

                 animate = () => {
                   this.animatedValue1.setValue(0);
                   this.animatedValue2.setValue(0);
                   this.animatedValue3.setValue(0);
                   const createAnimation = function(
                     value,
                     duration,

                     delay = 0,
                   ) {
                     return Animated.timing(value, {
                       toValue: 1,
                       duration,
                     
                       delay,
                     });
                   };
                   Animated.parallel([
                     createAnimation(
                       this.animatedValue1,
                       2000,
                      
                     ),
                     createAnimation(
                       this.animatedValue2,
                       1000,
                       
                       1000,
                     ),
                     createAnimation(
                       this.animatedValue3,
                       1000,
                       
                       2000,
                     ),
                   ]).start();
  }
  
  otpVerification = () => {
    
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch(
          'https://sites.mobotics.in/my-shop/api/register/send_otp',
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/x-www-form-urlencoded',
            },
             body: "mobile=" + this.state.mobile_number
          },
        )
          .then(response => response.json())
          .then(result => {
           
            if (result.success) {
             
              ToastAndroid.show(
                `${result.message}`,
                ToastAndroid.SHORT,
              );
              setTimeout(() => {
                this.props.navigation.navigate('OtpVerify', {
                   user_number:this.state.mobile_number
                 });
               
             }, 4000)
             
            } else {
              this.setState({
                button_loading: false,
                validation:true
              })
           }
         
          })
          .catch(error);
      } 
    })
  }

                 render() {
                  const scaleText = this.animatedValue1.interpolate(
                    {
                      inputRange: [0, 1],
                      outputRange: [0.5, 2],
                    },
                  );
                  const spinText = this.animatedValue2.interpolate(
                    {
                      inputRange: [0, 1],
                      outputRange: ['0deg', '720deg'],
                    },
                  );
                  const introButton = this.animatedValue3.interpolate(
                    {
                      inputRange: [0, 1],
                      outputRange: [-100, 400],
                    },
                  )
                   return (
                     <AnimatedSplash
                       translucent={false}
                       isLoaded={this.state.isLoading}
                       logoImage={require('../image/logo.jpg')}
                       backgroundColor={'#fdfefe'}
                       logoHeight={150}
                       logoWidht={150}>
                       <View
                         style={{
                           flex: 1,
                           justifyContent: 'center',
                           alignItems: 'center',
                         }}>
                         <StatusBar
                           barStyle="dark-content"
                           hidden={false}
                           translucent={true}
                         />
                         <Text style={{ fontSize: 16, fontWeight: 'bold' }} >To place order We need to Verify Your Number</Text>

                         <Input
                           placeholder="Enter Number"
                           leftIcon={{ type: 'font-awesome', name: 'phone', color:'#3f51b5' }}
                           leftIconContainerStyle={{
                             color:'blue'
                           }}
                           labelStyle={{
                             color:'blue100'
                           }}
                           
                           onChangeText={value => this.setState({ mobile_number: value })}
                         />

                         <Button title="Send" onPress={() => this.otpVerification() } containerStyle={{
                           backgroundColor: '#3f51b5',
                           width:300
                         }}
                           buttonStyle={{
                             backgroundColor:'#3f51b5'
                         }}
                         />
                       </View>
                     </AnimatedSplash>
                   );
                 }
}
  414718