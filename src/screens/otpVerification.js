import React, {Component} from 'react';

import { StyleSheet, View, Text, TouchableOpacity, Animated, AsyncStorage, Dimensions } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import { Input, Button, Card } from 'react-native-elements';

import NetInfo from '@react-native-community/netinfo';

const { width } = Dimensions.get("window");
const size = width - 32;
const storkeWidth = 50;
const radius = (size - storkeWidth) / 2;
const circuleSize = radius * 2;

export default class OtpVerification extends Component {
  constructor(props) {
    super(props)

    this.state = {
      input_otp:""
    }

    this.animatedValue1 = new Animated.Value(0);
    this.animatedValue2 = new Animated.Value(0);
    this.animatedValue3 = new Animated.Value(0);
  }

  componentDidMount() {
    this.animate();
  }
  
  verifyOtp = () => {
    var mobile_number = this.props.route.params.user_number;
  
    NetInfo.fetch().then(state => {

      if (state.isConnected) {
        fetch('https://sites.mobotics.in/my-shop/api/register/verify_otp', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
          },
           body: "otp=" + this.state.input_otp + " & mobile="+  this.props.route.params.user_number
        }).then(response => response.json())
          .then(result => {

            if (result.success) {
              AsyncStorage.setItem('user_id', JSON.stringify(result.user_id));

              this.props.navigation.navigate("LocationSelect");
            }
          }).catch(error => {
            console.log(error);
          });
      }
    });
  }
  animate = () => {
    this.animatedValue1.setValue(0);
    this.animatedValue2.setValue(0);
    this.animatedValue3.setValue(0);
    const createAnimation = function (
      value,
      duration,

      delay = 0,
    ) {
      return Animated.timing(value, {
        toValue: 1,
        duration,

        delay,
      });
    };
    Animated.parallel([
      createAnimation(
        this.animatedValue1,
        2000,

      ),
      createAnimation(
        this.animatedValue2,
        1000,

        1000,
      ),
      createAnimation(
        this.animatedValue3,
        1000,

        2000,
      ),
    ]).start();
  }
  render() {
                   
    const scaleText = this.animatedValue1.interpolate(
      {
        inputRange: [0, 1],
        outputRange: [0.5, 1],
      },
    );
    const spinText = this.animatedValue2.interpolate(
      {
        inputRange: [0, 1],
        outputRange: ['0deg', '720deg'],
      },
    );
    const introButton = this.animatedValue3.interpolate(
      {
        inputRange: [0, 1],
        outputRange: [-100, 400],
      },
    )
                   return (
                     <View
                       style={{
                         flex: 1,
                         justifyContent: 'center',
                         alignItems: 'center',
                         backgroundColor: '#fdfefe',
                       }}>
                       <Text
                         style={{
                           textAlign: 'center',
                           fontSize: 18,
                           fontWeight: 'bold',
                         }}>
                         {' '}
                         Please Enter Your Otp{' '}
                       </Text>

                       <Input
                         placeholder="Enter your otp"
                         leftIcon={{
                           type: 'font-awesome',
                           name: 'phone',
                           color: '#3f51b5',
                         }}
                         containerStyle={{
                           marginTop: 50,
                         }}
                         onChangeText={value =>
                           this.setState({input_otp: value})
                         }
                       />
                       <View
                         style={{
                           flexDirection: 'row',
                           marginTop: 20,
                         }}>
                         <TouchableOpacity>
                           <Text style={{color: '#3f51b5'}}>
                             Resend
                           </Text>
                         </TouchableOpacity>
                         <View
                           style={{
                             marginLeft: 80,
                             justifyContent: 'space-around',
                           }}>
                           <TouchableOpacity>
                             <Text
                               style={{color: '#3f51b5'}}>
                               Timer
                             </Text>
                           </TouchableOpacity>
                         </View>
                       </View>
                       <Button
                         onPress={() => this.verifyOtp()}
                         title=" Send "
                         buttonStyle={{
                           backgroundColor: '#3f51b5',
                         }}
                         containerStyle={{
                           width: 300,
                           marginTop: 20,
                           marginBottom: 60,
                         }}
                       />
                     </View>
                   );
                 }
  }