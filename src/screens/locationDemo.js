import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, Image, Animated, AsyncStorage } from 'react-native';

import { Card, Input, Button } from 'react-native-elements';

import NetInfo from '@react-native-community/netinfo';

export default class LocationSelection extends Component {

	constructor(props) {
		super(props);

		this.state = {
			location_input: '',
			isLoading:false
		}
		
		this.animatedValue1 = new Animated.Value(0);
		this.animatedValue2 = new Animated.Value(0);
		this.animatedValue3 = new Animated.Value(0);
	}

	componentDidMount() {
		this.animate();
	}


	animate = () => {
		this.animatedValue1.setValue(0);
		this.animatedValue2.setValue(0);
		this.animatedValue3.setValue(0);
		const createAnimation = function (
			value,
			duration,

			delay = 0,
		) {
			return Animated.timing(value, {
				toValue: 1,
				duration,

				delay,
			});
		};
		Animated.parallel([
			createAnimation(
				this.animatedValue1,
				2000,

			),
			createAnimation(
				this.animatedValue2,
				1000,

				1000,
			),
			createAnimation(
				this.animatedValue3,
				1000,

				2000,
			),
		]).start();
	}

	selectLocation = () => {
	
		NetInfo.fetch().then(status => {
			if (status.isConnected) {
				fetch('https://sites.mobotics.in/my-shop/api/delivery/access', {
					method: 'POST',
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/x-www-form-urlencoded',
					},
					body: "pincode=" + this.state.location_input
				}).then(response => response.json())
					.then(result => {
						if (result.status) {
							this.setState({
								isLoading:true
							})
						}
						AsyncStorage.setItem('notified', 'yes');
				this.props.navigation.navigate("HomeScreen")
					}).catch(error => {
						console.log(error);
				})
			}
		})
	}


	render() {

		const scaleText = this.animatedValue1.interpolate(
			{
				inputRange: [0, 1],
				outputRange: [0.5, 0.9],
			},
		);
		const spinText = this.animatedValue2.interpolate(
			{
				inputRange: [0, 1],
				outputRange: ['0deg', '720deg'],
			},
		);
		const introButton = this.animatedValue3.interpolate(
			{
				inputRange: [0, 1],
				outputRange: [-100, 400],
			},
		)
		return (
			<View style={{  height:700, alignItems: 'center', justifyContent:'center',  backgroundColor: '#fdfefe' }}  >
				<Image source={require('../image/location.jpg')} style={{ width: 350, height: 150, marginTop:10 }} />
				<Text style={{ textAlign: 'center', marginTop: 25, fontSize: 18 }} > Select Your Location</Text>


				<Input
					placeholder="Select location"
					leftIcon={{ type: 'font-awesome', name: 'map-marker', color: '#3f51b5' }}
					keyboardType='number-pad'
					onChangeText={value => this.setState({ comment: value })}
					containerStyle={{
						marginTop: 40
					}}
				/>
				<Button title="Select " onPress={() => this.selectLocation() } containerStyle={{
					width: 300, 
					marginTop: 38,
					marginBottom:100
				}}
					buttonStyle={{
						backgroundColor:'#3f51b5'
				}}
				/> 
			</View>
		)
	}
}

