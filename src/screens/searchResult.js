import React, { Component } from 'react';

import { StyleSheet, TouchableOpacity, View,  } from 'react-native';

import NetInfo from '@react-native-community/netinfo';

import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';

export default class SearchComponent extends Component {

	constructor(props) {
		super(props)

		this.state = {
			searchResult: []
		}
	}

	componentDidMount() {
		var value = this.props.route.params.searchValue;
		NetInfo.fetch().then(status => {
			if (status.isConnected) {
				fetch('http://sites.mobotics.in/my-shop/api/search?q=' + value, {
					method: 'GET'
				}).then(response => response.json())
					.then(result => {
						this.setState({
							searchResult: result.data
						})
					}).catch(error => {
						// console.log(error);
					});
			}
		});
	}

	render() {
		console.log(this.state.searchResult)
		return (
		
				<Container>
					
				<Content>

					
					<List
								dataArray={this.state.searchResult}
								renderRow={(items) => {
									if (items !="") {
										return (
											<View>
												<TouchableOpacity>
													<ListItem thumbnail onPress={() => alert("success")}>
														<Left>
															<Thumbnail square source={{ uri: items.image.small }} />
														</Left>
														<Body>
															<Text> {items.title} </Text>
															<Text note numberOfLines={1}> {items.description} </Text>
														</Body>
														<Right>

														</Right>
													</ListItem>
												</TouchableOpacity>
											</View>
										)
									} else {
										return (
											<View style={{ flex:1 }} >
												<Text noted> No Data Found </Text>
											</View>
										)
									}
								}}
							>

							</List>
				</Content>
				</Container>
			
		)
	}
}