import React, { Component } from 'react';

import { StyleSheet, View, TouchableOpacity, FlatList, ActivityIndicator,  } from 'react-native';

import NetInfo from '@react-native-community/netinfo';

import { Rating } from 'react-native-elements';

import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';

export default class ShopByCategory extends Component {

	constructor(props) {
		super(props)

		this.state = {
			checkNetworkStatus: false,
			catProducts: [],
			isLoading:false
		}
	}

	componentDidMount() {
		this.productByCategory();
	}

	productByCategory = () => {
		
		NetInfo.fetch().then(status => {
			if (status.isConnected) {
				fetch('https://sites.mobotics.in/my-shop/api/category/' + this.props.route.params.value, {
					method: 'GET'
				}).then(response => response.json())
					.then(result => {
						if (result) {
							this.setState({
								isLoading: true
							});
							this.setState({
								catProducts: result.data,
								checkNetworkStatus: true
							})
						}
					}).then(error => {
						console.log(error);
					});
			}
		});
	}

	showProducts = (data) => {
		
		this.props.navigation.navigate("ProductsDetails", {
			productData: data.slug,
			categories:data.categories
		});
	}

	render() {
		
		return (
			
			<Container>
				{
					this.state.isLoading ? (
						<Content>
							<List
								dataArray={this.state.catProducts}
								renderRow={(items, index) => {
									console.log(items)
									return (
										<ListItem ListItem thumbnail >
											<Left>
												<Thumbnail square source={{ uri: items.image.large }} />
											</Left>
											<Body>
												<Text>{items.title}</Text>
												<Rating
													imageSize={13}
													onFinishRating={this.ratingCompleted}
													style={{ marginRight: 120 }}
												/>
												<View style={{ flexDirection: 'row' }} >
													<Text note style={{ fontSize: 18, fontWeight: 'bold' }} >Price :</Text>
													<View style={{ flexDirection: 'row' }} >
														<Text style={{ marginTop: 2 }} >{items.price}₹  </Text>
														
													</View>
												</View>
											</Body>
											<Right>
												<Button transparent onPress={() => this.showProducts(items)} >
													<Text>View</Text>
												</Button>
											</Right>
										</ListItem>
									)
								}}
							>

							</List>
						</Content>
					) : (
							<View>
								<ActivityIndicator
									animating={true}
									style={{
										marginTop: 250,
										justifyContent: 'center',
										alignItems: 'center',
									}}
									size="large"
									color="#3f51b5"
								/>
							</View>
					)
				}
			</Container>
			
		)
	}
}