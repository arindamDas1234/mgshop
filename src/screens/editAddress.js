import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  Dimensions, KeyboardAvoidingView,
  AsyncStorage
} from 'react-native';

import {
  Header, Divider, Card, CheckBox, Button, Input
} from 'react-native-elements';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

import { IconButton, Colors, ActivityIndicator } from 'react-native-paper';

import NetInfo from '@react-native-community/netinfo';

import RadioGroup from 'react-native-custom-radio-group';
import { Value } from 'react-native-reanimated';

import AwesomeAlert from 'react-native-awesome-alerts';
import parseErrorStack from 'react-native/Libraries/Core/Devtools/parseErrorStack';




export default class Addaddress extends Component {
  constructor(props){
    super(props)

    this.state = {
      radioGroupList: [
        {
          "label": "Home",
          "value":"Home"
        },
        {
          "label": "Office",
          "value":"Office"
        },
        {
          "label": "Work",
          "value":"work"
        },

      ],
      status: false,
      nick_name_value: "",
      username_validation: true,
      add_validation: true,
      pincode_validation: true,
      landmark_validation: true,
      nick_name_validation: true,
      username: "",
      add: "",
      pincode: "",
      landMark: "",
      Ins: "",
      show_alert: false,
      alert_message: "",
      
      edit_address:{},
      type: "",
      isLoading: false,
      pin:""
    }
  }

  componentDidMount() {
    this.setState({
      pincode: this.props.route.params.address_value.pin,
      edit_address:this.props.route.params.address_value
    })
  }
 
  checkValue = (value) => {
    if (value == "work") {
        this.setState({
          status:true
        })
    } else {
      this.setState({
        nick_name_value: value,
        status:false
      })
     }
  } 
  
  editAddress = () => {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        AsyncStorage.getItem('user_id').then(result => {
          if (result) {
            fetch('https://sites.mobotics.in/my-shop/api/address', {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
              },
              body:
                'user_id=' +
                result +
                '&name=' +
                this.state.username +
                '&address=' +
                this.state.add +
                '&pin=' +
                this.state.pincode +
                '&land_mark=' +
                this.state.landMark +
                '&delivery_instructions=' +
                this.state.Ins +
                '&nick_name_for_address=' +
                this.state.nick_name_value,
            }).then(result => {
              if (result) {
                console.log(result);
              }
            }).catch(error => {
              console.log(error);
            })
           }
        })
      }
    })
  }

  getAddress = () => {
    var value = this.props.route.params.address_value;
    this.setState({
      edit_address: value
     
    })
    if (this.state.type == "work") {
      this.setState({
        status:true
      })
    }
  }
  render() {
    console.log(this.state.edit_address)
		return (
      <View style={{backgroundColor: '#fdfefe'}}>
        <View style={{backgroundColor: '#fdfefe', height:1000}}>
          <StatusBar
            barStyle="dark-content"
            hidden={false}
            translucent={true}
          />
          <Header
            placement="left"
            leftComponent={
              <IconButton
                icon="arrow-left"
                onPress={this.handleDrawer}
                color={Colors.blue800}
                size={20}
                onPress={() => this.props.navigation.goBack(null)}
              />
            }
            centerComponent={
              <Text
                style={{
                  textAlign: 'center',
                  marginHorizontal: 60,
                  fontSize: 20,
                  fontWeight: 'bold',
                }}>
                Add Address
              </Text>
            }
            containerStyle={{
              backgroundColor: '#fff',
            }}
          />
          {
            this.state.edit_address ? (
              <KeyboardAvoidingView>
                {this.state.username_validation ? (
                  <Input
                    placeholder="Enter username"
                    value={this.props.route.params.address_value.name}
                    onChangeText={(text) => this.setState({username:text}) }
                  />
                ) : (
                    <Input
                      placeholder="Enter username"

                      onChangeText={value => this.setState({ username: value })}
                      errorMessage="Please Enter Your valid User name"
                      errorStyle="red"
                    />
                  )}
                {this.state.add_validation ? (
                  <Input
                    placeholder="Enter Complete Address"
                    value={this.state.edit_address.address}
                    onChangeText={value => this.setState({ add: value })}
                  />
                ) : (
                    <Input
                      placeholder="Enter Complete Address"
                      onChangeText={value => this.setState({ add: value })}
                      errorMessage="Please Enter Your Valid Address"
                      errorStyle="red"
                    />
                  )}

                {this.state.landmark_validation ? (
                  <Input
                    placeholder="Pine code"
                    value={this.props.route.params.address_value.pin.toString()}
                    onChangeText={value => this.setState({ pincode: value })}
                  />
                ) : (
                    <Input
                      placeholder="Pine code"
                      onChangeText={value => this.setState({ pincode: value })}
                      errorMessage="Please Enter Your Valid Pincode"
                    />
                  )}

                {this.state.landmark_validation ? (
                  <Input
                    placeholder="Enter LandMark"
                    value={this.state.edit_address.land_mark}
                    onChangeText={value => this.setState({ landMark: value })}
                  />
                ) : (
                    <Input
                      placeholder="Enter LandMark"
                      onChangeText={value => this.setState({ landMark: value })}
                      errorMessage="Please Enter A valid land Mark"
                      errorStyle="red"
                    />
                  )}

                <Input
                  placeholder="Delivery Instructions"
                  value={this.state.edit_address.delivery_instructions}
                  onChangeText={value => this.setState({ Ins: value })}
                />
                <Text style={{ marginLeft: 10 }}>Nick name for Address</Text>

                <View style={{ marginTop: 20 }}>
                  <RadioGroup
                    radioGroupList={this.state.radioGroupList}
                    onChange={value => {
                      this.checkValue(value);
                    }}
                    initialValue={this.state.edit_address.type}
                    buttonContainerActiveStyle={{ backgroundColor: '#4267B2' }}
                    buttonTextActiveStyle={{ color: '#ffffff', fontFamily: 'lucida grande' }}
                    buttonContainerInactiveStyle={{ backgroundColor: '#dddddd' }}
                    buttonTextInactiveStyle={{ color: '#888888', fontFamily: 'lucida grande' }}
                    buttonContainerStyle={{ color: '#dddddd', backgroundColor: '#dddddd', borderColor: '#ffffff', margin: 5, width: 111 }}
                  />

                  {this.state.status ? (
                    <Input placeholder="Enter Your Address Details" />
                  ) : (
                      <View style={{ flex: 1, marginTop: 10 }}>
                        <ActivityIndicator color="#fff" />
                      </View>
                    )}
                </View>
                <Button
                  title=" Save Address "
                  onPress={this.editAddress}
                  type="outline"
                  containerStyle={{
                    width: 300,
                    marginLeft: 28,
                    marginTop: 30,
                  }}
                />
              </KeyboardAvoidingView>
            ): (
                <ActivityIndicator animating color='blue' />
             )
          }
        </View>
        <AwesomeAlert
          show={this.state.show_alert}
          showProgress={ this.state.show_alert }
          title="AwesomeAlert"
          message={this.state.alert_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="No, cancel"
          confirmText="Yes, delete it"
          confirmButtonColor="#DD6B55"
          onCancelPressed={() => {
            this.setState({
              show_alert: false,
            });
          }}
          onConfirmPressed={() => {
            this.setState({
              show_alert:false
            })
          }}
        />
      </View>
    );
	}
}