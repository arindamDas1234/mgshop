import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, StatusBar, FlatList, AsyncStorage, Image, SafeAreaView, TextInput } from 'react-native';

import { Header, Button, Divider, Input  } from 'react-native-elements';
import { Container, Content, List, ListItem, Thumbnail, Left, Body, Right,  } from 'native-base';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

import { IconButton, Colors } from 'react-native-paper';
import NetInfo from '@react-native-community/netinfo';
import { useIsFocused } from '@react-navigation/native';



import NumericInput from 'react-native-numeric-input'
import { ScrollView } from 'react-native-gesture-handler';

export default class CartShow extends Component {

  constructor(props) {
    super(props)

    this.state = {
      cartData: [],
      isLoading: false,
      isConnected: false,
      quantity: 1,
      totalPrice:""
    }
  
  }

  componentDidMount() {
    alert("Hy")
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
    this.getCart()
      
   });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  // shouldComponentUpdate() {
  //   this.getCart();
  // }

  checkList = () => {
    if (this.state.cartData.length >0) {
      return true;
    } else {
      return false;
    }
  }
  getCart = () => {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        this.setState({
          isConnected: true
        })

        AsyncStorage.getItem('user_id').then(result => {
          if (result) {
            fetch('https://sites.mobotics.in/my-shop/api/cart/items?user_id=' + result, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json'
              }
            }).then(response => response.json())
              .then(result => {
                this.setState({
                  cartData: result.data.items,
                  isLoading: true,
                  totalPrice:result.data.total
                })
                
              }).catch(error => {
                console.log(error);
              })
          }
        })
      }
    })
  }

  onPressDeleteItem = (data) => {

    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        AsyncStorage.getItem('user_id').then(result => {
          if (result) {
            fetch('https://sites.mobotics.in/my-shop/api/cart/items', {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
              },
              body: "product_price_id=" + data + "&user_id=" + result + "&quantity="+this.state.quantity
            }).then(response => response.json())
              .then(result => {
                this.setState({
                  cartData: result.data
                })
                this.getCart()
              }).catch(error => {
                console.log(error);
            })
            }
          })
       }
     })
  }

 

  render() {
    const { cartData } = this.state
  

    return (
     
      <View style={{ flex: 1, backgroundColor: '#fdfefe' }} >
        <StatusBar
          barStyle="light-content"
          hidden={false}
          translucent={true}
        />
        <Header
          placement="left"
          leftComponent={
            <IconButton
              icon="arrow-left"
              onPress={this.handleDrawer}
              color={Colors.blue800}
              size={20}
              onPress = {() => this.props.navigation.goBack(null) }
            />
          }
          centerComponent={
           <Text style={{ textAlign:'center', marginHorizontal:50, fontSize:20, fontWeight:'bold' }} > Shopping Cart </Text>
         }
         
          containerStyle={{
            backgroundColor: '#fff',
          }}
        />

        {
          this.checkList() ? (
            <View style={{ flex: 1 }} >
              
             
<SafeAreaView style={styles.container}>
                <ScrollView style={{ height:'100%' }} >
                  <View style={{ marginBottom: 100 }} >
                    {/* <View style={styles.titleContainer}>
                        <Text style={styles.title}>Jewellery</Text>
                    </View> */}
                    <ScrollView  >
                      <FlatList
                        data={cartData}
                        renderItem={(item) => {
                          return (
                            <View style={styles.itemContainer} >
                              <View style={styles.listItem} >
                                <Image
                                  style={styles.image}
                                  source={{ uri: item.item.product_image.small }}
                                />
                                <View style={styles.content}>
                                  <View style={styles.details}>
                                    <Text style={styles.adTitle}>{item.item.product_name}</Text>
                                    <Text style={styles.qty}>Qty: {item.item.quantity} </Text>
                                    <Text style={styles.price}>VND {item.item.product_price * item.item.quantity} </Text>
                                  </View>
                                  <View style={styles.closeBtnWrapper}>
                                    <TouchableOpacity onPress={() => this.onPressDeleteItem(item.item.product_price_detail.id)}>
                                      <Icon name="close" size={20} color="#3f51b5" />
                                    </TouchableOpacity>
                                  </View>


                                </View>
                              </View>
                            </View>
                          )
                        }}
                      />

                    </ScrollView>

                    <View>
                      <TextInput
                        placeholder="Enter your PromoCode"
                        style={{
                          backgroundColor: '#f3f3f3',
                          width: 300,
                          marginLeft: 30,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderColor: '#fff',

                        }}
                        placeholderTextColor='black'
                      />
                      <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }} >
                        <Text style={{ marginTop: 4, marginLeft: 28 }} > Delivery Charges </Text>
                        <View style={{ marginHorizontal: 60, marginTop: 3 }} >
                          <Text>Free</Text>
                        </View>
                      </View>
                      <Divider style={{ backgroundColor: 'grey', marginTop: 10 }} />
                      <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }} >
                        <Text style={{ fontWeight: 'bold', fontSize: 16, marginRight: 30 }} >Total Price</Text>
                        <View style={{ marginRight: 30 }} >
                          <Text style={{ fontWeight: 'bold', fontSize: 16, }}>₹ {this.state.totalPrice} </Text>
                        </View>
                      </View>

                      <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }} >
                        <Text style={{ color: 'grey', marginRight: 48 }} >Total Items</Text>
                        <View style={{ marginRight: 38 }} >
                          <Text style={{ color: 'grey' }} >3</Text>
                        </View>
                      </View>
                    </View>

                  </View>
                </ScrollView>
                  <TouchableOpacity style={styles.buyMeButton}>

                    <View style={{ flexDirection: 'row', }} >
                      <Button
                        onPress={() => this.props.navigation.navigate("addressShow")}
                        title="Check Out"
                        type="outline"
                        containerStyle={{
                          width: 300,
                          marginRight: 20,
                          backgroundColor: '#fff',
                          borderColor:'#3f51b5'
                      }}
                      
                      buttonStyle={{
                        backgroundColor: '#fff',
                        borderColor: '#3f51b5',
                      }}
                      titleStyle={{
                        color: '#3f51b5',
                      }}
                      />
                    </View>
                  </TouchableOpacity>
               
              </SafeAreaView>
              
            </View>
          ) : (
              <View style={{
                backgroundColor: '#fdfefe' }} >
                
                <Image source={{ uri: 'https://www.kindpng.com/picc/m/174-1749310_shopping-cart-png-empty-shopping-cart-png-transparent.png' }} style={{ height: 300, width: '100%', alignItems: 'center', marginTop: 30 }} />
                <View style={{ flexDirection: 'row', marginLeft:50 }} >
                  <Text style={{ textAlign: 'center', marginTop: 20, marginRight:-15 }} >Your Cart is Empty Please Go to Shop  </Text> 
                  <View style={{ marginLeft:30, marginTop:15 }} >
                    <Image source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRyiqlzuCqhHVccgGbf5hITfh2dUN_MnL0Xtw&usqp=CAU' }}
                      style={{
                        height: 30,
                        width:30
                    }}
                    />
  </View>
                </View>
                <Button title="Shop Now" onPress={() => this.props.navigation.navigate("HomeScreen")} type="outline" containerStyle={{
                 marginTop:20,
                  width: 200,
                  marginLeft: 80,
                  borderColor:'#3f51b5'
               }} 
                  buttonStyle={{
                    backgroundColor: '#fff',
                    borderColor:'#3f51b5'
                  }}
                  
                  titleStyle={{
                    color:'#3f51b5'
                  }}
                />
              </View>
          )
        }

        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    color: '#fff'
  },
  listItem: {
    height: 120,
    margin: 10,
    flexDirection: 'row',
  },
  image: {
    height: 80,
    width: 100,
    flex: 1,
    margin: 10
  },
  itemContainer: {
    height: 130,
    borderBottomWidth: 1,
    borderBottomColor: '#c6c6c6'
  },
  content: {
    flex: 2,
    flexDirection: 'row',
    margin: 10
  },
  details: {
    flex: 2,
  },
  closeBtnWrapper: {
    justifyContent: 'center',
    alignItems: 'center',

  },
  closeBtn: {
    height: 50,
    width: 50,
    borderRadius: 90,
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#F44336',
    justifyContent: 'center',

  },
  qty: {
    fontWeight: '400',
    fontSize: 15,
  },
  price: {
    fontWeight: '400',
    fontSize: 15,
    color: 'red'
  },
  adTitle: {
    fontWeight: '600',
    fontSize: 16,
    margin: 5
  },

  adContent: {

  },
  buttonContainer: {

  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  buyMeButton: {
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    textAlign: 'center',
    alignItems: 'center',
    padding: 20

  },
  buttonText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '500'
  }
})

const mapStateToProps = (state) => {
  return {
    products: state.productReducer
  };
};

