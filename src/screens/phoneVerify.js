import React, { Component } from 'react';

import {
	StyleSheet,
	View,
	Text,
	Animated,
	TouchableOpacity,
	ToastAndroid,
	Alert
} from 'react-native';

import AnimatedSplash from "react-native-animated-splash-screen";

// import HomeChild Screens /

import HomeChildScreens from './HomeChildScreens';

//  import Login Screen 

import LoginScreen from './Login';

import { SharedElement } from 'react-native-motion';

import Icon from 'react-native-vector-icons/FontAwesome';
import { Input, Button } from 'react-native-elements';

import NetInfo from '@react-native-community/netinfo';

import Toast from 'react-native-toast-message';

export default class FlashMessages extends Component {
	constructor(props) {
		super(props);

		this.state = {
			button_loading: false,
			checkConnection: false,
			mobile_number: '',
			validation: false
		};

		this.animatedValue1 = new Animated.Value(0);
		this.animatedValue2 = new Animated.Value(0);
		this.animatedValue3 = new Animated.Value(0);
	}

	// componentWillMount() {

	// }

	componentDidMount() {
		setTimeout(() => {
			this.setState({
				isLoading: true,
			});
			this.animate();
		}, 7500);

	}

	animate = () => {
		this.animatedValue1.setValue(0);
		this.animatedValue2.setValue(0);
		this.animatedValue3.setValue(0);
		const createAnimation = function (
			value,
			duration,

			delay = 0,
		) {
			return Animated.timing(value, {
				toValue: 1,
				duration,

				delay,
			});
		};
		Animated.parallel([
			createAnimation(
				this.animatedValue1,
				2000,

			),
			createAnimation(
				this.animatedValue2,
				1000,

				1000,
			),
			createAnimation(
				this.animatedValue3,
				1000,

				2000,
			),
		]).start();
	}

	otpVerification = () => {
		alert("success")
		NetInfo.fetch().then(status => {
			if (status.isConnected) {
				fetch(
					'https://sites.mobotics.in/my-shop/api/register/send_otp',
					{
						method: 'POST',
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/x-www-form-urlencoded',
						},
						body: "mobile=" + this.state.mobile_number
					},
				)
					.then(response => response.json())
					.then(result => {

						if (result.success) {

							ToastAndroid.show(
								`${result.message}`,
								ToastAndroid.SHORT,
							);
							setTimeout(() => {
								this.props.navigation.navigate('OtpVerify', {
									user_number: this.state.mobile_number
								});

							}, 4000)

						} else {
							this.setState({
								button_loading: false,
								validation: true
							})
						}

					})
					.catch(error);
			}
		})
	}

	render() {
		const scaleText = this.animatedValue1.interpolate(
			{
				inputRange: [0, 1],
				outputRange: [0.5, 2],
			},
		);
		const spinText = this.animatedValue2.interpolate(
			{
				inputRange: [0, 1],
				outputRange: ['0deg', '720deg'],
			},
		);
		const introButton = this.animatedValue3.interpolate(
			{
				inputRange: [0, 1],
				outputRange: [-100, 400],
			},
		)
		return (
			// <AnimatedSplash
			// 	translucent={false}
			// 	isLoaded={this.state.isLoading}
			// 	logoImage={{
			// 		uri:
			// 			'https://previews.123rf.com/images/distrologo/distrologo1902/distrologo190200290/117595386-shopping-logo-design-template-shopping-bag-icon-design.jpg',
			// 	}}
			// 	backgroundColor={'#fff'}
			// 	logoHeight={150}
			// 	logoWidht={150}>
				
			// </AnimatedSplash>

			<View
				style={{
					flex: 1,
					justifyContent: 'center',
					alignItems: 'center',
				}}>
				<Animated.View
					style={{
						transform: [{ scale: scaleText }],
					}}>
					<Text
						style={{
							color: 'grey',
							fontSize: 12,
						}}>
						{' '}
                             WelCome To My Grocery App{' '}
					</Text>
				</Animated.View>
				<Animated.View
					style={{
						marginTop: 20,
						transform: [{ rotate: spinText }],
					}}>
					<Text style={{ fontSize: 12 }}>
						Verify Your Mobile Number
                           </Text>
				</Animated.View>
				{this.state.validation == false ? (
					<Input
						containerStyle={{
							width: 300,
							marginTop: 30,
						}}
						onChangeText={value =>
							this.setState({
								mobile_number: value,
							})
						}
						keyboardType="number-pad"
						placeholder="Enter Your Mobile Number"
					/>
				) : (
						<Input
							containerStyle={{
								width: 300,
								marginTop: 30,
							}}
							onChangeText={value =>
								this.setState({
									mobile_number: value,
								})
							}
							keyboardType="number-pad"
							placeholder="Enter Your Mobile Number"
							errorStyle={{ color: 'red' }}
							errorMessage="Invalid Enter Input"
						/>
					)}

				{this.state.button_loading ==
					false ? (
						<Button
							title="Verify Your Number"
							onPress={this.otpVerification}
						/>
					) : (
						<Button
							title="Verify Your Number"
							onPress={this.otpVerification}
							loading
						/>
					)}
			</View>
		);
	}
}
414718