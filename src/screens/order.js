import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, ScrollView, Image, StatusBar, FlatList, AsyncStorage, PermissionsAndroid } from 'react-native';

import { Header, Button, Divider, Input } from 'react-native-elements';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

// import RNFetchBlob from 'react-native-fetch-blob';

import { IconButton, Colors } from 'react-native-paper';
import NetInfo from '@react-native-community/netinfo';


import DialogBox from 'react-native-dialogbox';

import RNFetchBlob from 'rn-fetch-blob';


export default class OrderScreen extends Component {

	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			ordersData: [],
			cartData: [],
			isCheckData: true,
		}
	}

	componentDidMount() {
		this._unsubscribe = this.props.navigation.addListener('focus',() => {
			this.getOrderData();
			this.cartData();
		} )
	}

	componentWillUnmount() {
		this._unsubscribe();
	}

	getOrderData = async () => {
	 await	AsyncStorage.getItem('user_id').then(user_id => {
			if (user_id) {
				NetInfo.fetch().then(status => {
					if (status.isConnected) {
						fetch('https://sites.mobotics.in/my-shop/api/orders?user_id=' + user_id, {
							method: 'GET'
						}).then(response => response.json())
							.then(result => {
								if (result.data.length > 0) {
									this.setState({
										ordersData: result.data,
										isLoading:true
									})
								} else {
									this.setState({
										isCheckData:false
									})
								}
							}).catch(error => {
								console.log(error);
							});
					} else {
						alert("No Internet Connection");
					}
				})
			}
		})
	}

	checkStatus = (status) => {
		if (status === "Payment Pending") {
			return require('../image/bluedot.png');
		} else if (status === "Delivered") {
			return require('../image/greendot.png');
		} else {
			return require('../image/reddot.png');
		}
	}

	cartData = async () => {
		await AsyncStorage.getItem('user_id').then(result => {
			if (result) {
				NetInfo.fetch().then(status => {
					if(status.isConnected){
						fetch('http://sites.mobotics.in/my-shop/api/cart/items?user_id=' + result, {
							method: 'GET'
						}).then(response => response.json())
							.then(result => {
								if (result.data.items.length > 0) {
									this.setState({
										cartData: reuslt.data.items,
										isLoading:true
									})
								} else {
									this.setState({
										isCheckData:false
									})
								}
							}).catch(error => {
								console.log(error);
							});
					} else {
						alert("No Internet Connection");
					}
				})
			}
		})
	}
	invoice = (order_id) => {
		this.dialogbox.confirm({
			title: 'Alert',
			content: ['Are You sure ? To Download The Invoice'],
			ok: {
				text: 'Yes',
				style: {
					color: 'green'
				}
			},
			
			cancel: {
				text: 'No',
				style: {
					color: 'red'
				}
			},
		}).then((event) => {
			if (event.button) {

				this.downloadFile(order_id);
			} else {
			}
		});
	}

	downloadFile = async (order_id) => {
		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
				title: 'Storage message',
				message: 'Pleace Allow The Storage Permission',
			}
			);

			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				this.requestPermission(order_id);
			} else {
				alert("This Application Need to Access Your Phone Sotorage");
			}
		} catch (error) {
			console.log(error);
		}
	}

	requestPermission =  async (order_id) => {
		await AsyncStorage.getItem('user_id').then(result => {
			if (result) {
				var date = new Date();
				let dirs = RNFetchBlob.fs.dirs
				RNFetchBlob
					.config({
						// response data will be saved to this path if it has access right.
						path: dirs.DocumentDir + '/invoice'+ order_id+ ".pdf"
					})
					.fetch('POST', 'http://sites.mobotics.in/my-shop/api/orders/download', {
						'Content-Type':'application/x-www-form-urlencoded'
					})
					.then((res) => {
						alert("File Save successfully" + res.path());
						this.props.navigation.navigate("Invoice", {
							value:result
						})
					})
			}
		})
	}

	orderStatus = (status, time) => {
		var timeStamp = Math.floor( Date.now() / 1000);
		
		if (timeStamp < Number(time)) {
			if (status === "Payment Pending") {
				return true;
			} else if (status === "Received") {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	editOrder = (order_id, cupon) => {
		if (cupon == "") {
			this.props.navigation.navigate("EditOrder", {
				cuper_value: cupon,
				order_id_value:order_id
			});
		} else {
			this.props.navigation.navigate("EditOrder", {
				cuper_value: cupon
			});
		}
	}

	checkStatus2 = ( status ) => {
		if (status === "Packed/Billed") {
			return true;
		} else if (status === "Approved") {
			return true;
		} else if (status === "Delivered") {
			return true;
		} else if (status === "Received") {
			return true;
		} else if (status == "Payment Pending") {
			return true;
		} else {
			return false;
		 }
	}

	render() {
		return (
			<View style={{ backgroundColor:'#fff' }} >
				<StatusBar
					barStyle="light-content"
					hidden={false}
					translucent={true}
					backgroundColor="#3f51b5"
				/>
				<Header
					placement="left"
					leftComponent={
						<IconButton
							icon="arrow-left"
							onPress={this.handleDrawer}
							color={Colors.blue800}
							size={20}
							onPress={ ()=> this.props.navigation.goBack(null)  }
						/>
					}
					centerComponent={
						<Text style={{ textAlign: 'center', marginHorizontal: 80, fontSize: 24, fontWeight: 'bold' }} > Orders </Text>
					}

					containerStyle={{
						backgroundColor: '#fff',
					}}
				/>
				<ScrollView style={{ height: '100%', marginBottom:200 }} >
					<View style={{ 
						height: 50,
						flexDirection: 'row',
						justifyContent: 'space-around',
						flex: 1, 
						borderWidth: 1,
						padding:2
					 }} >
						<View style={{ 
							flex: 1,
							borderWidth: 1,
							alignItems: 'center',
							 justifyContent:'center'
						}} >
							<View style={{ flexDirection: 'row', justifyContent: 'space-around'  }} >
								<Image source={require('../image/bluedot.png')} style={{ height: 10, width: 10, marginTop: 4, marginRight:10}} />
								<View>
									<Text style={{ color:'grey' }}  > pending </Text>
						</View>
				</View>
						</View>
						
						<View style={{
							flex: 1,
							borderWidth: 1,
							alignItems: 'center',
							justifyContent: 'center'
						}} >
							<View style={{ flexDirection: 'row', justifyContent:'space-around' }} >
								<Image source={require('../image/reddot.png')} style={{ height: 10, width: 10, marginTop: 4, marginRight:10 }} />
								
								<View style={{ justifyContent:'space-around' }} >
									<Text style={{ color: 'grey', marginRight:10 }} >Canceled</Text>
						</View>
							</View>
						</View>
						<View style={{
							flex: 1,
							borderWidth: 1,
							alignItems: 'center',
							justifyContent: 'center'
						}} >
							<View style={{ flexDirection: 'row', justifyContent: 'space-around' }} >
								<Image source={require('../image/greendot.png')} style={{ height: 10, width: 10, marginRight: 10, marginTop:4 }} />
								<View>
									<Text style={{ color: 'grey' }} >Completed</Text>
						</View>
							</View>
						</View>
					</View>

					{
						this.state.ordersData.length > 0 ? (
							<FlatList style={{ height: '100%', marginBottom:130 }} data={this.state.ordersData}
								renderItem={(items) => {
									return (
                    <View
                      elevation={1}
                      style={{
                        marginLeft: 30,
                        width: '100%',
                        height: 130,
                        width: '100%',
                        margin: 5,
                        backgroundColor: '#fff',
                        border: 2.9,
                        borderColor: 'black',
                        alignSelf: 'center',
                        shadowColor: '#000',
                        shadowOffset: {
                          width: 0,
                          height: 16,
                        },
                        shadowOpacity: 1,
                        shadowRadius: 7.49,
                      }}>
                      <View style={{flexDirection: 'row'}}>
                        <View style={{flexDirection: 'row'}}>
                          <Image
                            style={{
                              height: 10,
                              width: 10,
                              marginRight: 5,
                              marginTop: 8,
                            }}
                            source={this.checkStatus(
                              items.item.status,
                            )}
                          />
                          <View>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                fontSize: 20,
                              }}>
                              {' '}
                              Order#:{' '}
                            </Text>
                          </View>
                        </View>
                        <View>
                          <Text style={{marginTop: 5}}>
                            
                            {items.item.order_id}
                          </Text>
                        </View>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={{color: 'grey'}}>
                          place on:
                        </Text>
                        <View>
                          <Text
                            style={{
                              color: 'grey',
                              marginRight: 5,
                            }}>
                            
						{items.item.ordered_on}
                          </Text>
                        </View>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={{color: 'grey'}}>
                          Status:
                        </Text>
                        <View>
                          <Text
                            style={{
                              color: 'grey',
                              marginRight: 5,
                            }}>
                            {' '}
                            {items.item.status}{' '}
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginTop: 15,
                          marginRight: 30,
                        }}>
                        {this.orderStatus(
                          items.item.status,
                          items.item.delivery_date_time,
                        ) ? (
                          <Button
                            title="Edit"
                            onPress={() => this.editOrder( items.item.order_id, items.item.cupon ) }
                            type="outline"
                            containerStyle={{
                              width: 100,
                              borderColor: '#3f51b5',
                            }}
                            buttonStyle={{
                              backgroundColor: '#fff',
                              borderColor: '#3f51b5',
                            }}
                            titleStyle={{
                              color: '#3f51b5',
                            }}
                          />
                        ) : (
                          <Button
                            title="Edit"
                            onPress={() =>
                              alert('Order Not Editable :(')
                            }
                            type="outline"
                            containerStyle={{
                              width: 100,
                              borderColor: '#3f51b5',
                            }}
                            buttonStyle={{
                              backgroundColor: '#e0e0e0',
                              borderColor: '#3f51b5',
                            }}
                            titleStyle={{
                              color: '#6573c3',
                            }}
                          />
                        )}
                        <View style={{marginLeft: 30}}>
                          {this.checkStatus2(
                            items.item.status,
                          ) ? (
                            <Button
                              title="Invoice"
                              onPress={() =>
                                this.invoice(
                                  items.item.order_id,
                                )
                              }
                              containerStyle={{
                                marginRight: 30,
                                width: 100,
                              }}
                              buttonStyle={{
                                backgroundColor: '#3f51b5',
                              }}
                              titleStyle={{
                                color: '#fff',
                              }}
                            />
                          ) : (
                            <Button
                              title="Invoice"
                              onPress={() =>
                                alert('Invoice not Possible')
                              }
                              containerStyle={{
                                marginRight: 30,
                                width: 100,
                              }}
                              buttonStyle={{
                                backgroundColor: '#6573c3',
                              }}
                              titleStyle={{
                                color: '#bdbdbd',
                              }}
                            />
                          )}
                        </View>
                        <View>
                          <TouchableOpacity>
                            <Image
                              source={require('../image/delete.png')}
                              style={{
                                height: 20,
                                width: 20,
                                marginTop: 5,
                              }}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  );
								}}
							/>
						): (
							null
						)
				}
				</ScrollView>
				<DialogBox ref={dialogbox => { this.dialogbox = dialogbox }} />
			</View>
		)
	}
}