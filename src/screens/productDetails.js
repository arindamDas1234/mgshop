import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  Image,
  Dimensions,
  ActivityIndicator,
  ScrollView,
  AsyncStorage,
  FlatList
} from 'react-native';

import {
  Header,
  Card,
  Button,
  Rating,
  AirbnbRating,
  Avatar,
  CheckBox
} from 'react-native-elements';

// import Icon from 'react-native-vector-icons/FontAwesome';

import { IconButton, Colors } from 'react-native-paper';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

import NetInfo from '@react-native-community/netinfo';

import FlashMessage from 'react-native-flash-message';

// import {showMessage, hideMessage} from 'react-native-flash-message';

import DropdownAlert from 'react-native-dropdownalert';

import RadioGroup from 'react-native-custom-radio-group';

// import ImageSlider from 'react-native-image-slider';
// import Cart from './cart';

export default class ProductDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {

      vendor_id: '',
      productDetails: [],
      product_image: '',
      product_price: [],
      price_id: '',
      products_details: [],
      relatedProducts: [],
      product_price_id: '',
      quantity: "1",
      cart_button_change: false,
      size: [],
      initialValue: "",
      variation: ""
    };
  }

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.productDetails();
      this.featcherProducts();
    })
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  productDetails = () => {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch(
          'https://sites.mobotics.in/my-shop/api/products/' +
          this.props.route.params.productData,
          {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
          .then(response => response.json())
          .then(result => {

            if (result) {

              this.setState({
                productDetails: result.data,
                product_image: result.data.media[0].large,
                product_price_id: result.data.prices[0].id,
                product_price: result.data.prices[0],
                initialValue: result.data.prices[0].size

              });
              result.data.prices.map(value => {
                console.log(value.size)
                this.setState({
                  size: value.size
                })
              })
            } else {
              alert('No Products Found');
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        alert('Please Check Your Internet Connection');
      }
    });
  }

  featcherProducts = () => {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch('https://sites.mobotics.in/my-shop/api/products/' + this.props.route.params.productData + '/related', {
          method: 'GET'
        }).then(response => response.json())
          .then(result => {
            console.log(result.data)
            if (result.data) {
              this.setState({
                relatedProducts: result.data
              })
            } else {
              alert("No Data Found");
            }
          }).catch(error => {
            console.log(error);
          });
      }
    })
  }

  radioCheck = () => {
    if (this.state.size.length > 1) {

      return true;
    } else {
      return false;
    }
  }
  addFeatureProducts = (data) => {
    this.props.navigation.navigate("FetcherProducts2", {
      productData: data
    })
  }

  productRatting = ratting => {
    console.log('The Ratting is ' + ratting);
  };

  addToCart = () => {
    alert(this.state.variation);
    console.log(this.state.product_price_id);
    AsyncStorage.getItem('user_id').then(result => {
      if (result) {

        NetInfo.fetch().then(status => {
          if (status.isConnected) {
            fetch('https://sites.mobotics.in/my-shop/api/cart/add', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
              },
              body: "product_price_id=" + this.state.product_price_id + "&quantity=" + this.state.quantity + "&user_id=" + result
            })
              .then(data => {
                console.log(data);
                if (data.status === 200) {
                  this.setState({
                    cart_button_change: true
                  });
                  this.dropDownAlertRef.alertWithType(
                    'success',
                    'Success',
                    'Product Added Success Fully',
                  );
                } else {
                  this.dropDownAlertRef.alertWithType(
                    'error',
                    'Error',
                    'Product All ready Exists in Cart',
                  );
                }
              }).catch(error => {
                console.log(error);
              });
          }
        })
      } else {

      }
    })
  }

 

  variationQuantity = (value) => {
    if (this.state.product_price_id != value) {
      // alert("No")
      this.setState({
        quantity: 1
      })
    }
  }

  render() {

    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
          hidden={false}
          translucent={true}
          backgroundColor="#3f51b5"
        />
        <ScrollView>
          <DropdownAlert ref={ref => (this.dropDownAlertRef = ref)} />
          {/* <StatusBar backgroundColor="#fff" barStyle="dark-content" /> */}
          {/* <Header
						placement="left"
						leftComponent={
							<TouchableOpacity
								onPress={() =>
									this.props.navigation.replace('shopByCategory')
								}>
								<Icon
									name="angle-left"
									size={28}
									containerStyle={{
										paddingRight: 10,
									}}
									color={Colors.blue800}
								/>
							</TouchableOpacity>
						}
						// centerComponent={{text: 'MY TITLE', style: {color:'blue'}}}
						rightComponent={
							<TouchableOpacity
								style={{ flexDirection: 'row', marginRight: 8 }}
								onPress={() => console.log('Press')}>
								<Icon name="search" size={20} color={Colors.blue800} />
								<TouchableOpacity style={{ marginLeft: 10 }}>
									<Icon name="shopping-cart" size={20} color={Colors.blue800} />
								</TouchableOpacity>
							</TouchableOpacity>
						}
						containerStyle={{
							backgroundColor: '#fff',
							justifyContent: 'space-around',
						}}
					/> */}

          <Image
            source={{ uri: this.state.product_image }}
            style={{
              height: 300,
              width: Dimensions.get('window').width,
              borderBottomWidth: 1,
            }}
          />
          <View style={{ marginTop: 15 }}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 10,
                marginLeft: 10,
              }}>
              {this.state.productDetails.title}
            </Text>
            <Text style={{ fontSize: 16, marginLeft: 15, marginTop: 13 }}>
              ₹ {this.state.product_price.total}/
              {this.state.product_price.size}
            </Text>

            <View>
              {
                this.radioCheck() ? (

                  <RadioGroup
                    radioGroupList={this.state.productDetails.prices.map(value => ({
                      label: value.size.substring(0, 18),
                      value: value.id
                    }))}
                    initialValue={this.state.initialValue.substring(0, 18)}
                    onChange={value => {
                      this.variationQuantity(value)
                    }}
                    buttonContainerActiveStyle={{ backgroundColor: '#4267B2' }}
                    buttonTextActiveStyle={{
                      color: '#ffffff',
                      fontFamily: 'lucida grande',
                    }}
                    buttonContainerInactiveStyle={{ backgroundColor: '#dddddd' }}
                    buttonTextInactiveStyle={{
                      color: '#888888',
                      fontFamily: 'lucida grande',
                    }}
                    buttonContainerStyle={{
                      color: '#dddddd',
                      backgroundColor: '#dddddd',
                      borderColor: '#ffffff',
                      margin: 5,
                      width: 111,
                    }}
                  />
                ) : (null)
              }
            </View>
            <View style={{ marginLeft: 85 }} />
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginTop: 24,
              justifyContent: 'space-around',
            }}>
            {/* {this.state.cart_button_change ? (
              <Button
                title="Go To Cart"
                containerStyle={{
                  width: 150,
                }}
                onPress={() => this.props.navigation.navigate('Cart',{ value:"status"})}
              />
            ) : (
              <Button
                title="ADD TO CART"
                type="outline"
                onPress={this.addToCart}
                containerStyle={{
                  width: 150,
                }}
              />
            )} */}

            {/* <View>
              <Button
                title="BUY NOW"
                containerStyle={{
                  width: 150,
                }}
              />
            </View> */}
          </View>

          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              marginTop: 20,
              marginLeft: 10,
            }}>
            Products Description
          </Text>
          <Text style={{ fontSize: 14, marginLeft: 12, marginTop: 20 }}>
            {this.state.productDetails.description}
          </Text>

          <Text
            style={{
              marginTop: 20,
              fontSize: 20,
              fontWeight: 'bold',
              marginBottom: 16,
            }}>
            {' '}
            Feature Products{' '}
          </Text>

          <FlatList
            data={this.state.relatedProducts}
            horizontal
            renderItem={(items, index) => {
              return (
                <TouchableOpacity
                  onPress={() => this.addFeatureProducts(items.item.slug)}>
                  <Card style={{ elevation: 10 }}>
                    <View>
                      <Text
                        style={{
                          fontSize: 16,
                          color: '#fff',
                          left: 10,
                          zIndex: 4000,
                          backgroundColor: 'green',
                          width: 60,
                          height: 20,
                          position: 'absolute',
                          marginLeft: 5,
                        }}>
                        10 % Off
                      </Text>
                      <Image
                        source={{ uri: items.item.image.large }}
                        style={{
                          height: 150,
                          width: 200,
                          justifyContent: 'space-around',
                          marginHorizontal: 15,
                          marginBottom: 15,
                        }}
                      />
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 20,
                          fontWeight: 'bold',
                        }}>
                        {' '}
                        {items.item.title}{' '}
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-evenly',
                          marginLeft: 15,
                        }}>
                        <Text
                          style={{
                            fontSize: 18,
                            fontWeight: 'bold',
                            marginTop: 10,
                          }}>
                          Price:
                        </Text>
                        <View style={{ justifyContent: 'space-evenly' }}>
                          <Text
                            style={{ textAlign: 'center', marginTop: 15 }}>
                            {items.item.price}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <Text
                          style={{
                            fontSize: 18,
                            marginLeft: 45,
                            fontWeight: 'bold',
                          }}>
                          {' '}
                          Delivery{' '}
                        </Text>
                        <View>
                          {items.item.fast_delivery == false ? (
                            // <Text>Hy</Text>
                            <CheckBox
                              size={13}
                              checkedColor="green"
                              containerStyle={{
                                marginLeft: 43,
                                justifyContent: 'space-around',
                                marginTop: -1,
                              }}
                              checked={true}
                            />
                          ) : (
                              <CheckBox
                                size={13}
                                uncheckedColor="red"
                                uncheckedIcon="square-o"
                                containerStyle={{
                                  marginLeft: 43,
                                  justifyContent: 'space-around',
                                  marginTop: -1,
                                }}
                                unchecked={true}
                              />
                            )}
                        </View>
                      </View>

                      <View />
                    </View>
                  </Card>
                </TouchableOpacity>
              );
            }}
          />




        </ScrollView>
        {/* <FlashMessage position="top" /> */}
        <TouchableOpacity style={styles.buyMeButton}>
          <View style={{ flexDirection: 'row' }}>
            <Button
              title=" Add To cart "
              onPress={() => this.addToCart()}
              buttonStyle={{
                backgroundColor: '#3f51b5',
                color: '#fff'
              }}
              containerStyle={{
                color: '#fff'
              }}

              containerStyle={{
                width: 300,
              }}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#fdfefe',
  },
  buyMeButton: {
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    textAlign: 'center',
    alignItems: 'center',
    padding: 20,
  },
});
