import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, Image, Dimensions, AsyncStorage, FlatList } from 'react-native';

import { Header, Divider, Card, CheckBox,Button } from 'react-native-elements';

import { Container, Content, List, ListItem, } from 'native-base';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

import Modal from 'react-native-modal';

import { IconButton, Colors, ActivityIndicator } from 'react-native-paper';

import NetInfo from '@react-native-community/netinfo';

import DialogBox from 'react-native-dialogbox';

export default class Address1 extends Component {
	
	constructor(props) {
		super(props);

		this.state = {
			isModal: false,
			check_data: []
		}
	}

	

	componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
       this.getAddress()
     })
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  getAddress = () => {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        AsyncStorage.getItem('user_id').then(result => {
          if (result) {
            fetch(
              'https://sites.mobotics.in/my-shop/api/address?user_id=' + result, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json'
              }
            }
            ).then(response => response.json())
              .then(result => {

                this.setState({
                  check_data: result
                })

              }).catch(error => {
                console.log(error);
              })
          }
        })
      }
    })
  }
  
  check_delivery = (data) => {
    
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch('https://sites.mobotics.in/demo-ecommerse/api/delivery/access', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: 'pincode=' + data.pin
        }).then(response => response.json())
          .then(result => {
            if (result.status) {
              this.props.navigation.navigate("Checkout", {
                delivery_date: result.delivery_date,
                delivery_time: result.delivery_time,
                delivery_Address: data.address,
                name: data.name,
                pincode: data.pin,
                value:data.id
              })
            }
          }).catch(error => {
            console.log(error);
          });
      } else {
        alert("No Internet connection");
       }
     })
  }
  deleteAdd = ( value) => {
            this.dialogbox.confirm({
              title: 'Alert',
              content: ['Are You Sure To Delete Address'],
              ok: {
                text: 'Yes',
                style: {
                  color: 'red',
                },
                callback: () => {
                 
                  NetInfo.fetch().then(status => {
                    if (status.isConnected) {
                      AsyncStorage.getItem('user_id').then(
                        result => {
                          if (result) {
                            fetch(
                              'https://sites.mobotics.in/my-shop/api/address?user_id=' +
                                result,
                              {
                                method: 'DELETE',
                                headers: {
                                  Accept: 'application/json',
                                  'Content-Type':
                                    'application/x-www-form-urlencoded',
                                },
                                body: 'address_id=' + value,
                              },
                            )
                              .then(response => response.json())
                              .then(result => {
                                if (result.success) {
                                  this.dialogbox.alert("Address Deleted successfully");
                                  
                                }
                              })
                              .catch(error => {
                                console.log(error);
                              });
                          }
                        },
                      );
                    }
                  });
                },
              },
              cancel: {
                text: 'No',
                style: {
                  color: 'blue',
                },
                callback: () => {
                  this.dialogbox.alert('Hurry up！');
                },
              },
            });
  }
  
  render() {
    console.log( this.state.check_data.length )
		return (
      <View style={{backgroundColor: '#fdfefe', height: 1000}}>
        <Header
          placement="left"
          leftComponent={
            <IconButton
              icon="arrow-left"
              onPress={this.handleDrawer}
              color={Colors.blue800}
              size={20}
              onPress={() => this.props.navigation.goBack(null)}
            />
          }
          centerComponent={
            <Text
              style={{
                textAlign: 'center',
                marginHorizontal: 60,
                fontSize: 20,
                fontWeight: 'bold',
              }}>
              Add Address
            </Text>
          }
          containerStyle={{
            backgroundColor: '#fff',
          }}
        />

        {this.state.check_data.length > 0 ? (
          <View style={{backgroundColor: '#fdfefe'}}>
            <Divider
              style={{backgroundColor: 'grey'}}
              style={{marginBottom: 15, marginTop: 20, elevation: 5}}
            />

            <FlatList
              data={this.state.check_data}
              style={{width: Dimensions.get('window').width + 50}}
              renderItem={items => {
                console.log(items.item);
                return (
                  <View>
                    <TouchableOpacity onPress={() => this.check_delivery(items.item)} >
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 30,
                          marginTop: 20,
                        }}>
                        <Text
                          style={{
                            textAlign: 'center',
                            fontSize: 13,
                            fontWeight: 'bold',
                          }}>
                          {' '}
                          Name:
                        </Text>

                        <View>
                          <Text style={{textAlign: 'center'}}>
                            {' '}
                            {items.item.name}{' '}
                          </Text>
                        </View>
                      </View>

                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 40,
                        }}>
                        <Text
                          style={{
                            textAlign: 'center',
                            fontSize: 12,
                            color: 'grey',
                          }}>
                          {' '}
                          pin:
                        </Text>

                        <View>
                          <Text
                            style={{
                              textAlign: 'center',
                              color: 'grey',
                            }}>
                            {' '}
                            {items.item.pin}{' '}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            marginHorizontal: 160,
                          }}>
                          <Icon
                            name="pencil"
                            color="grey"
                            onPress={() =>
                              this.props.navigation.navigate(
                                'EditAddress',
                                {
                                  address_value: items.item,
                                },
                              )
                            }
                            style={{marginRight: 30}}
                            size={20}
                          />
                          <View>
                            <Icon
                              name="ban"
                              color="grey"
                              size={20}
                              onPress={() =>
                                this.deleteAdd(items.item.id)
                              }
                            />
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                );
              }}
            />

            <Button
              title="Add new Address "
              onPress={() => this.props.navigation.navigate('Addaddress')}
              containerStyle={{
                marginTop: 80,
                width: 200,
                marginLeft: 80,
              }}
              type="outline"
            />
          </View>
        ) : (
          <View
            style={{
              backgroundColor: '#fdfefe',
              height: Dimensions.get('screen').height + 100,
            }}>
            <Image
              source={require('../image/empty.png')}
              style={{width: 111, height: 111, marginHorizontal: 40}}
            />
            <Text style={{textAlign: 'center', marginTop: 30}}>
              Oops! No Address. You have to add address to continue
            </Text>

            <Button
              title="Address "
              onPress={() => this.props.navigation.navigate('Addaddress')}
              containerStyle={{
                marginTop: 80,
                width: 200,
                marginLeft: 80,
              }}
              type="outline"
            />
          </View>
        )}
        <DialogBox
          ref={dialogbox => {
            this.dialogbox = dialogbox;
          }}
        />
      </View>
    );
	}
}